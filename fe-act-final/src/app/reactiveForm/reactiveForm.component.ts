import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { ValidationService } from '../shared/validation.service';
import { Hero } from '../shared/hero';
import { ACRemoteConfiguration } from 'slarn-autocomplete';
import {ACLocalConfiguration} from 'slarn-autocomplete';
import { itemService } from "../items/item.service";
import { Observable,throwError } from 'rxjs/';




@Component({
  selector: 'reactive-driven-form',
  templateUrl: './reactiveForm.component.html'
})
export class ReactiveFormComponent implements OnInit {


  heroForm: FormGroup;
  model: Hero; 
  submittedModel: Hero; 
  powers: string[];
  submitted: boolean = false;
  userData: any[] = [];
  errorMessage: string;
  slarn_local_config: ACLocalConfiguration = {
    template: `
      <div><strong>#name#</strong></div>
      <div>#email#</div>
    `,
    key: 'id',
    value: 'name',
    data: this.userData,
    
    
  };
  
  disabled: boolean = false;

  constructor(private formBuilder: FormBuilder,
    private itemService: itemService) { 
     
    }
  
  ngOnInit() {
    this.itemService.getEmployee().subscribe(
      items => {
        this.userData =  items;
          console.log( "test" +JSON.stringify( this.userData));
       });

      this.model = new Hero(18, 'Dr IQ', 'Really Smart', 'Chuck Overstreet', 'iq@superhero.com');
      
      this.powers = ['Really Smart', 'Super Flexible', 
                     'Hypersound', 'Weather Changer'];                     
                     
      this.heroForm = this.formBuilder.group({
        name:     [this.model.name, Validators.required],
        alterEgo: [this.model.alterEgo, Validators.required],
        email:    [this.model.email, [Validators.required, ValidationService.emailValidator]],
        power:    [this.model.power, Validators.required]
      });


      
      
  }

  onSubmit({ value, valid }: { value: Hero, valid: boolean }) {
    this.submitted = true;
    this.submittedModel = value;
  }

  getEmployeelist(): any[]{
    
   this.itemService.getEmployee().subscribe(
    items => {
      return  items;
        console.log( "test" +JSON.stringify( this.userData));
     },

    error => this.errorMessage = <any>error
   ); 
    
    console.log('USERDATE FROM FUNCION' + this.userData)
      return this.userData;

  } 


}