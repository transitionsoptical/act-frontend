import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html'
})
export class ModalComponent implements OnInit {
  popupMessage: string;
  showLoading: boolean = false;
  showMessage: boolean = false;
  redirectUrl: string = ''; 
  constructor(private route: ActivatedRoute,
    private router: Router,) {
    
  }

  ngOnInit() {
    console.log(this.popupMessage);
    this.redirectUrl = '';
  }
  onOk() {
    this.showMessage = false;
    if (this.redirectUrl.length > 0)
      this.router.navigateByUrl(this.redirectUrl);
  }
}
