import { OnInit, Component } from "@angular/core";
import { FormBuilder, FormGroup } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { AuthService } from "../services/auth.service";
import { itemService } from '../items/item.service';
import { IItem2, AttachementDetails } from "../items/item";
import { tick } from "@angular/core/src/render3";

@Component({
    selector: 'display',
    templateUrl: './display.component.html'

})

export class DisplayComponent implements OnInit {
    displayForm: FormGroup;

    public showEhsapp: boolean;
    public showEhsrev: boolean;
    public showSPRev: boolean;
    public showBU1: boolean;
    public showBU2: boolean;
    public showFinApp1: boolean;
    public showGlobalFinApp1: boolean;
    public showFA1: boolean;
    public showFA2: boolean;
    public showFA3: boolean;
    public showFA4: boolean;
    public showFA5: boolean;
    public showFA6: boolean;
    public showFA7: boolean;
    public showExecApp:boolean;
    public showCFOApp:boolean;
    public showRegFin:boolean;
    public showBusUnit: boolean;
    public showBusApp:boolean;
    public showGlobalFinApp:boolean;
    public showEhsappbtn: boolean;
    public showEhsrevbtn: boolean;
    public showSPRevbtn: boolean;
    public showBU1btn: boolean;
    public showBU2btn: boolean;
    // public showFinApp1btn: boolean = true;
    public showFin1btn: boolean;
    public showGlobalFinApp1btn: boolean;
    public showFA1btn: boolean;
    public showFA2btn: boolean;
    public showFA3btn: boolean;
    public showFA4btn: boolean;
    public showFA5btn: boolean;
    public showFA6btn: boolean;

    public showehsdet: boolean;
    public isCurrent: boolean;

    businessUnits: IItem2[] = [];
    listOfFilesAffectedSaved: AttachementDetails[] = [];


    userEmpList: any[] = [];

    constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private itemService: itemService,
        private authService: AuthService
    ) {

    }

    ngOnInit(): void {
        this.displayForm = this.formBuilder.group({
            id: [0],
            projectName: [''],
            projectManagerName: [''],
            location: [''],
            region:[''],
            entity:[''],
            classification: [''],
            projectDescription: [''],
            startContruction: [''],
            startUpDate: [''],
            economicLife: [''],
            projectIRR: [''],
            projectCashPaybackYears: [''],
            projectROC: [''],
            status: [''],
            actAmount: [''],
            actNumber: [''],
            doesNeedEHSReview: [''],
            invesmentCategory: [''],
            budgetCategory: [''],
            actAccountNumber: [''],
            ehsApproverEmail: [''],
            ehsReviewerEmail: [''],
            globalFinanceApprover1Email: [''],
            businessUnitApprover1Email: [''],
            businessUnitApprover2Email: [''],
            finalApprover1Email: [''],
            finalApprover2Email: [''],
            finalApprover3Email: [''],
            finalApprover4Email: [''],
            finalApprover5Email: [''],
            finalApprover6Email: [''],
            financeEmail: [''],
            purchasingEmail: [''],
            financeApprover1Email: [''],
            spReviewerEmail: [''],
            wfCurrentApprover: [''],
            wfCurrentApproverEmail: [''],
            wfCurrentApproverName: [''],
            wfehsApprover: [''],
            wfehsApproverApproval: [''],
            wfehsApproverComment: [''],
            wfehsReviewer: [''],
            wfehsReviewerApproval: [''],
            wfehsReviewerComment: [''],
            wfspReviewer: [''],
            wfspReviewerApproval: [''],
            wfspReviewerComment: [''],
            wfbU1Approver: [''],
            wfbU1ApproverApproval: [''],
            wfbU1ApproverComment: [''],
            wfbU2Approver: [''],
            wfbU2ApproverApproval: [''],
            wfbU2ApproverComment: [''],
            wfFinanceApprover1: [''],
            wfFinanceApprover1Approval: [''],
            wfFinanceApprover1Comment: [''],
            wfGlobalFinanceApprover1: [''],
            wfGlobalFinanceApprover1Approval: [''],
            wfGlobalFinanceApprover1Comment: [''],
            wfFinalApprover1: [''],
            wfFinalApprover1Approval: [''],
            wfFinalApprover1Comment: [''],
            wfFinalApprover2: [''],
            wfFinalApprover2Approval: [''],
            wfFinalApprover2Comment: [''],
            wfFinalApprover3: [''],
            wfFinalApprover3Approval: [''],
            wfFinalApprover3Comment: [''],
            wfFinalApprover4: [''],
            wfFinalApprover4Approval: [''],
            wfFinalApprover4Comment: [''],
            wfFinalApprover5: [''],
            wfFinalApprover5Approval: [''],
            wfFinalApprover5Comment: [''],
            wfFinalApprover6: [''],
            wfFinalApprover6Approval: [''],
            wfFinalApprover6Comment: [''],
            wfApprover1Approval: [''],
            wfApprover1ApprovalDetails: [''],
            wfApprover1Comment: [''],
            wfApprover2Approval: [''],
            wfApprover2ApprovalDetails: [''],
            wfApprover2Comment: [''],
            wfApprover3Approval: [''],
            wfApprover3ApprovalDetails: [''],
            wfApprover3Comment: [''],
            wfApprover4Approval: [''],
            wfApprover4ApprovalDetails: [''],
            wfApprover4Comment: [''],
            wfApprover5Approval: [''],
            wfApprover5ApprovalDetails: [''],
            wfApprover5Comment: [''],
            wfApprover6Approval: [''],
            wfApprover6ApprovalDetails: [''],
            wfApprover6Comment: [''],
            wfApprover7Approval: [''],
            wfApprover7ApprovalDetails: [''],
            wfApprover7Comment: [''],
            wfbU1Approval: [''],
            wfbU1ApprovalDetails: [''],
            wfbU1Comment: [''],
            wfbU2Approval: [''],
            wfbU2ApprovalDetails: [''],
            wfbU2Comment: [''],
            wfbU3Approval: [''],
            wfbU3ApprovalDetails: [''],
            wfbU3Comment: [''],
            wfClassificationByDivisionApproval: [''],
            wfDivisionApprovalDetails: [''],
            wfClassificationbyDivisionComment: [''],
            wfehsApproval: [''],
            wfehsApprovalDetails: [''],
            wfehsApproverApprovalDetails: [''],
            wfehsComment: [''],
            wfehsReviewer2: [''],
            wfehsReviewer2Details: [''],
            wfehsReviewer2Comment: [''],
            wfFinance1Approval: [''],
            wfFinance1ApprovalDetails: [''],
            wfFinance1Comment: [''],
            wfFinance2Approval: [''],
            wfFinance2ApprovalDetails: [''],
            wfFinance2Comment: [''],
            wfPriorDivApproval: [''],
            wfPriorDivApprovalDetails: [''],
            wfPriorDivComment: [''],
            created: ['']
        });

        this.showBU1 = true;
        this.showBU2 = true;
        this.showFinApp1 = true;
        this.showEhsapp = true;
        this.showSPRev = true;
        this.showEhsrev = true;
        this.showGlobalFinApp1 = true;
        this.showFA1 = true;
        this.showFA2 = true;
        this.showFA3 = true;
        this.showFA4 = true;
        this.showFA5 = true;
        this.showFA6 = true;
        this.showFA7 = true;
        this.showExecApp = true;
        this.showCFOApp = true;
        this.showRegFin = true;
        this.showBusUnit = true;
        this.showBusApp = true;
        this.showGlobalFinApp = true;
        this.showEhsrevbtn = false;
        this.showFin1btn = false;
        this.showSPRevbtn = false;
        this.showBU1btn = false;
        this.showBU2btn = false;
        this.showGlobalFinApp1btn = false;
        this.showFA1btn = false;
        this.showFA2btn = false;
        this.showFA3btn = false;
        this.showFA4btn = false;
        this.showFA5btn = false;
        this.showFA6btn = false;

        this.itemService.getEmployee().subscribe(
            res => {
                this.userEmpList = res;

                const param = this.route.snapshot.paramMap.get('id');
                if (param) {
                    const reqid = +param;
                    this.getItem(reqid.toString());
                }

            });


    }
    private getItem(reqid: string) {
        this.itemService.getACTs(reqid).subscribe(
            item => {
                this.displayForm.patchValue(item);
                console.log(this.displayForm.value);
                var _actamount = parseInt(this.displayForm.value.actAmount);

                this.itemService.getAttachmentDetails(Number(reqid)).subscribe(
                    res => {
                        this.listOfFilesAffectedSaved = res.filter(x1 => x1.additionalInfo.includes('FinanceACT'));

                    }
                )
                if (this.displayForm.value.doesNeedEHSReview == false) {
                    this.showEhsapp = false;
                    this.showEhsrev = false;

                    if (this.displayForm.value.wfCurrentApprover == "S&PReviewer") {
                        let x = this.userEmpList.filter(x => x.email == this.authService.email)
                        if (x.length > 0) {
                            if (x[0].name == this.displayForm.value.wfspReviewer || x[0].name == "Silva, Coren")
                                this.showSPRevbtn = true;
                        }
                    }
                }

                let newDate = new Date(this.displayForm.value.created);
                let appDate = new Date('07-30-2019');
                let appDate2 = new Date('09-16-2020');

                if (this.displayForm.value.wfehsApprover == "" || this.displayForm.value.wfehsApprover == null) {
                    this.showEhsapp = false;
                }
                if (this.displayForm.value.wfehsReviewer == "" || this.displayForm.value.wfehsReviewer == null) {
                    this.showEhsrev = false;
                }
                if (this.displayForm.value.wfspReviewer == "" || this.displayForm.value.wfspReviewer == null) {
                    this.showSPRev = false;
                }
                if (this.displayForm.value.wfbU1Approver == "" || this.displayForm.value.wfbU1Approver == null) {
                    this.showBU1 = false;
                    this.showBusUnit = false;
                }
                if (this.displayForm.value.wfbU2Approver == "" || this.displayForm.value.wfbU2Approver == null) {
                    this.showBU2 = false;
                    this.showBusApp = false;
                }
                if (this.displayForm.value.wfFinanceApprover1 == "" || this.displayForm.value.wfFinanceApprover1 == null) {
                    this.showFinApp1 = false;
                    this.showRegFin = true;
                    
                }
                else if (this.displayForm.value.wfFinanceApprover1 != null || this.displayForm.value.wfFinanceApprover1 != "") {
                    if (newDate < appDate) {
                        this.showFinApp1 = true;
                    }
                }

                if (this.displayForm.value.wfGlobalFinanceApprover1 == "" || this.displayForm.value.wfGlobalFinanceApprover1 == null) {
                    this.showGlobalFinApp1 = false;
                    this.showGlobalFinApp = false;
                }
                else if (this.displayForm.value.wfGlobalFinanceApprover1 != null || this.displayForm.value.wfGlobalFinanceApprover1 != "") {
                    if (newDate < appDate) {
                        this.showGlobalFinApp1 = true;
                    }
                }

                if (this.displayForm.value.wfFinalApprover1 == "" || this.displayForm.value.wfFinalApprover1 == null) {
                    this.showFA1 = false;
                }
                else if (this.displayForm.value.wfFinalApprover1 != null || this.displayForm.value.wfFinalApprover1 != "") {
                    if (newDate < appDate) {
                        this.showFA1 = true;
                    }
                }

                if (this.displayForm.value.wfFinalApprover2 == "" || this.displayForm.value.wfFinalApprover2 == null) {
                    this.showFA2 = false;
                }
                else if (this.displayForm.value.wfFinalApprover2 != null || this.displayForm.value.wfFinalApprover2 != "") {
                    if (newDate < appDate) {
                        this.showFA2 = true;
                    }
                }

                if (this.displayForm.value.wfFinalApprover3 == "" || this.displayForm.value.wfFinalApprover3 == null) {
                    this.showFA3 = false;
                }
                else if (this.displayForm.value.wfFinalApprover3 != null || this.displayForm.value.wfFinalApprover3 != "") {
                    if (newDate < appDate) {
                        this.showFA3 = true;
                    }
                }

                if (this.displayForm.value.wfFinalApprover4 == "" || this.displayForm.value.wfFinalApprover4 == null) {
                    this.showFA4 = false;
                }
                else if (this.displayForm.value.wfFinalApprover4 != null || this.displayForm.value.wfFinalApprover4 != "") {
                    if (newDate < appDate) {
                        this.showFA4 = true;
                    }

                }

                if (this.displayForm.value.wfFinalApprover5 == "" || this.displayForm.value.wfFinalApprover5 == null) {
                    this.showFA5 = false;
                    this.showExecApp = false;
                }
                else if (this.displayForm.value.wfFinalApprover5 != null || this.displayForm.value.wfFinalApprover5 != "") {
                    if (newDate < appDate) {
                        this.showFA5 = true;
                    }
                }

                if (this.displayForm.value.wfFinalApprover6 == "" || this.displayForm.value.wfFinalApprover6 == null) {
                    this.showFA6 = false;
                    this.showCFOApp = false;
                }
                else if (this.displayForm.value.wfFinalApprover6 != null || this.displayForm.value.wfFinalApprover6 != "") {
                    if (newDate < appDate) {
                        this.showFA6 = true;
                    }
                }
                if(newDate >= appDate2){
                    this.isCurrent = true;
                    if (_actamount <= 50000) {
                        this.showExecApp = false;
                        this.showCFOApp = false;
                    }
                }
                else if (newDate >= appDate && newDate <= appDate2) {
                    this.isCurrent = false;
                    if (_actamount <= 50000) {
                        this.showGlobalFinApp1 = false;
                    }
                    if (_actamount <= 100000) {
                        this.showFA1 = false;
                    }
                    if (_actamount <= 250000) {
                        this.showFA2 = false;
                    }
                    if (_actamount <= 500000) {
                        this.showFA3 = false;
                        this.showFA4 = false;
                    }
                    if (_actamount <= 2000000) {
                        this.showFA5 = false;
                    }
                    if (_actamount <= 3000000) {
                        this.showFA6 = false;
                    }
                    this.showRegFin = false;
                    this.showExecApp = false;
                    this.showCFOApp = false;
                    this.showBusUnit = false;
                    this.showBusApp = false;
                    this.showGlobalFinApp = false;
                }
                else {
                    this.isCurrent = false;
                    if (!(this.displayForm.value.wfehsApprovalDetails == null || this.displayForm.value.wfehsApprovalDetails == ""))
                        this.displayForm.controls['wfehsApprover'].disable();

                    if (!(this.displayForm.value.wfehsReviewer2Details == null || this.displayForm.value.wfehsReviewer2Details == ""))
                        this.displayForm.controls['wfehsReviewer'].disable();

                    if (!(this.displayForm.value.wfbU3ApprovalDetails == null || this.displayForm.value.wfbU3ApprovalDetails == ""))
                        this.displayForm.controls['wfspReviewer'].disable();

                    if (!(this.displayForm.value.wfbU1ApprovalDetails == null || this.displayForm.value.wfbU1ApprovalDetails == ""))
                        this.displayForm.controls['wfbU1Approver'].disable();

                    if (!(this.displayForm.value.wfbU2ApprovalDetails == null || this.displayForm.value.wfbU2ApprovalDetails == ""))
                        this.displayForm.controls['wfbU2Approver'].disable();

                    if (!(this.displayForm.value.wfFinance1ApprovalDetails == null || this.displayForm.value.wfFinance1ApprovalDetails == ""))
                        this.displayForm.controls['wfFinanceApprover1'].disable();

                    if (!(this.displayForm.value.wfFinance2ApprovalDetails == null || this.displayForm.value.wfFinance2ApprovalDetails == ""))
                        this.displayForm.controls['wfGlobalFinanceApprover1'].disable();

                    if (!(this.displayForm.value.wfApprover1ApprovalDetails == null || this.displayForm.value.wfApprover1ApprovalDetails == ""))
                        this.displayForm.controls['wfFinalApprover1'].disable();

                    if (!(this.displayForm.value.wfApprover2ApprovalDetails == null || this.displayForm.value.wfApprover2ApprovalDetails == ""))
                        this.displayForm.controls['wfFinalApprover2'].disable();

                    if (!(this.displayForm.value.wfApprover3ApprovalDetails == null || this.displayForm.value.wfApprover3ApprovalDetails == ""))
                        this.displayForm.controls['wfFinalApprover3'].disable();

                    if (!(this.displayForm.value.wfApprover4ApprovalDetails == null || this.displayForm.value.wfApprover4ApprovalDetails == ""))
                        this.displayForm.controls['wfFinalApprover4'].disable();

                    if (!(this.displayForm.value.wfApprover5ApprovalDetails == null || this.displayForm.value.wfApprover5ApprovalDetails == ""))
                        this.displayForm.controls['wfFinalApprover5'].disable();

                    if (!(this.displayForm.value.wfApprover6ApprovalDetails == null || this.displayForm.value.wfApprover6ApprovalDetails == ""))
                        this.displayForm.controls['wfFinalApprover6'].disable();


                    if (!(this.displayForm.value.wfehsApproval == null || this.displayForm.value.wfehsApproval == ""))
                        this.displayForm.controls['wfehsApproverApproval'].disable();

                    if (!(this.displayForm.value.wfehsReviewer2 == null || this.displayForm.value.wfehsReviewer2 == ""))
                        this.displayForm.controls['wfehsReviewerApproval'].disable();

                    if (!(this.displayForm.value.wfbU3Approval == null || this.displayForm.value.wfbU3Approval == ""))
                        this.displayForm.controls['wfspReviewerApproval'].disable();
                    if (!(this.displayForm.value.wfbU1Approval == null || this.displayForm.value.wfbU1Approval == ""))
                        this.displayForm.controls['wfbU1ApproverApproval'].disable();

                    if (!(this.displayForm.value.wfbU2Approval == null || this.displayForm.value.wfbU2Approval == ""))
                        this.displayForm.controls['wfbU2ApproverApproval'].disable();

                    if (!(this.displayForm.value.wfFinance1Approval == null || this.displayForm.value.wfFinance1Approval == ""))
                        this.displayForm.controls['wfFinanceApprover1Approval'].disable();

                    if (!(this.displayForm.value.wfFinance2Approval == null || this.displayForm.value.wfFinance2Approval == ""))
                        this.displayForm.controls['wfGlobalFinanceApprover1Approval'].disable();

                    if (!(this.displayForm.value.wfApprover1Approval == null || this.displayForm.value.wfApprover1Approval == ""))
                        this.displayForm.controls['wfFinalApprover1Approval'].disable();

                    if (!(this.displayForm.value.wfApprover2Approval == null || this.displayForm.value.wfApprover2Approval == ""))
                        this.displayForm.controls['wfFinalApprover2Approval'].disable();

                    if (!(this.displayForm.value.wfApprover3Approval == null || this.displayForm.value.wfApprover3Approval == ""))
                        this.displayForm.controls['wfFinalApprover3Approval'].disable();

                    if (!(this.displayForm.value.wfApprover4Approval == null || this.displayForm.value.wfApprover4Approval == ""))
                        this.displayForm.controls['wfFinalApprover4Approval'].disable();

                    if (!(this.displayForm.value.wfApprover5Approval == null || this.displayForm.value.wfApprover5Approval == ""))
                        this.displayForm.controls['wfFinalApprover5Approval'].disable();

                    if (!(this.displayForm.value.wfApprover6Approval == null || this.displayForm.value.wfApprover6Approval == ""))
                        this.displayForm.controls['wfFinalApprover6Approval'].disable();
                }


                if (this.displayForm.value.wfCurrentApprover == "EHSApprover" || this.displayForm.value.wfCurrentApprover == "EHSApp") {
                    if (!(this.displayForm.value.wfehsApproverApproval == "Approved" || this.displayForm.value.wfehsApproval == "Approve")) {
                        let x = this.userEmpList.filter(x => x.email == this.authService.email)
                        if (x.length > 0) {
                            if (x[0].name == this.displayForm.value.wfehsApprover || x[0].name == "Silva, Coren")
                                this.showEhsappbtn = true;
                        }
                    }
                }

                else if (this.displayForm.value.wfCurrentApprover == "EHSReviewer" || this.displayForm.value.wfCurrentApprover == "EHS2") {
                    if ((this.displayForm.value.wfehsReviewerApproval != "Approved" && this.displayForm.value.wfehsApproverApproval == "Approved") ||
                        (this.displayForm.value.wfehsReviewer2 != "Approve" && this.displayForm.value.wfehsApproverApproval == "Approve")) {

                        let x = this.userEmpList.filter(x => x.email == this.authService.email)
                        if (x.length > 0) {
                           if (x[0].name == this.displayForm.value.wfehsReviewer)
                                this.showEhsrevbtn = true;
                        }
                    }
                }

                else if (this.displayForm.value.wfCurrentApprover == "S&PReviewer" || this.displayForm.value.wfCurrentApprover == "BU3") {
                    if ((this.displayForm.value.wfspReviewerApproval != "Approved" && this.displayForm.value.wfehsReviewerApproval == "Approved") ||
                        (this.displayForm.value.wfbU3Approval != "Approve" && this.displayForm.value.wfehsReviewer2 == "Approve")) {
                        let x = this.userEmpList.filter(x => x.email == this.authService.email)
                        if (x.length > 0) {
                            if (x[0].name == this.displayForm.value.wfspReviewer || x[0].name == "Silva, Coren")
                                this.showSPRevbtn = true;
                        }
                    }
                }
                else if (this.displayForm.value.wfCurrentApprover == "BU1Approver" || this.displayForm.value.wfCurrentApprover == "BU1") {
                    if ((this.displayForm.value.wfbU1ApproverApproval != "Approved" && this.displayForm.value.wfspReviewerApproval == "Approved") ||
                        (this.displayForm.value.wfbU1Approval != "Approve" && this.displayForm.value.wfbU3Approval == "Approve")) {
                        let x = this.userEmpList.filter(x => x.email == this.authService.email)
                        if (x.length > 0) {
                            if (x[0].name == this.displayForm.value.wfbU1Approver || x[0].name == "Silva, Coren")
                                this.showBU1btn = true;
                        }
                    }
                }
                else if (this.displayForm.value.wfCurrentApprover == "FinanceApprover1" || this.displayForm.value.wfCurrentApprover == "F1") {
                    if ((this.displayForm.value.wfFinanceApprover1Approval != "Approved" && this.displayForm.value.wfbU1ApproverApproval == "Approved") ||
                        (this.displayForm.value.wfbU2Approval != "Approve" && this.displayForm.value.wfbU1Approval == "Approve")) {
                        let x = this.userEmpList.filter(x => x.email == this.authService.email)
                        if (x.length > 0) {
                            if (x[0].name == this.displayForm.value.wfFinanceApprover1 || x[0].name == "Silva, Coren")
                            this.showFin1btn = true;
                                
                        }
                    }

                }
                else if (this.displayForm.value.wfCurrentApprover == "BU2Approver" || this.displayForm.value.wfCurrentApprover == "BU2") {
                    if (this.displayForm.value.wfbU2ApproverApproval != "Approved" && this.displayForm.value.wfFinanceApprover1Approval == "Approved") {

                        let x = this.userEmpList.filter(x => x.email == this.authService.email)
                        if (x.length > 0) {
                            if (x[0].name == this.displayForm.value.wfbU2Approver || x[0].name == "Silva, Coren")
                            this.showBU2btn = true;

                        }
                    }

                }
                else if (this.displayForm.value.wfCurrentApprover == "GlobalFinanceApprover1" || this.displayForm.value.wfCurrentApprover == "F2") {
                    if (this.displayForm.value.wfGlobalFinanceApprover1Approval != "Approved" && this.displayForm.value.wfbU2ApproverApproval == "Approved") {
                        // if (_actamount > 50000) {
                            let x = this.userEmpList.filter(x => x.email == this.authService.email)
                            if (x.length > 0) {
                                if (x[0].name == this.displayForm.value.wfGlobalFinanceApprover1 || x[0].name == "Silva, Coren")
                                    this.showGlobalFinApp1btn = true;
                            }
                        // }
                    }
                }
                // else if (this.displayForm.value.wfCurrentApprover == "FinalApprover1" || this.displayForm.value.wfCurrentApprover == "A1") {
                //     if (this.displayForm.value.wfFinalApprover1Approval != "Approved" && this.displayForm.value.wfGlobalFinanceApprover1Approval == "Approved") {

                //         if (_actamount > 100000) {
                //             let x = this.userEmpList.filter(x => x.email == this.authService.email)
                //             if (x.length > 0) {
                //                 if (x[0].name == this.displayForm.value.wfFinalApprover1)
                //                     this.showFA1btn = true;
                //             }
                //         }
                //     }
                // }
                // else if (this.displayForm.value.wfCurrentApprover == "FinalApprover2" || this.displayForm.value.wfCurrentApprover == "A2") {
                //     if (this.displayForm.value.wfFinalApprover2Approval != "Approved" && this.displayForm.value.wfFinalApprover1Approval == "Approved") {

                //         if (_actamount > 250000) {
                //             let x = this.userEmpList.filter(x => x.email == this.authService.email)
                //             if (x.length > 0) {
                //                 if (x[0].name == this.displayForm.value.wfFinalApprover2)
                //                     this.showFA2btn = true;
                //             }
                //         }
                //     }
                // }
                // else if (this.displayForm.value.wfCurrentApprover == "FinalApprover3" || this.displayForm.value.wfCurrentApprover == "A3") {
                //     if (this.displayForm.value.wfFinalApprover3Approval != "Approved" && this.displayForm.value.wfFinalApprover2Approval == "Approved")

                //         if (_actamount > 500000) {
                //             let x = this.userEmpList.filter(x => x.email == this.authService.email)
                //             if (x.length > 0) {
                //                 if (x[0].name == this.displayForm.value.wfFinalApprover3)
                //                     this.showFA3btn = true;
                //             }
                //         }
                // }
                // else if (this.displayForm.value.wfCurrentApprover == "FinalApprover4" || this.displayForm.value.wfCurrentApprover == "A4") {
                //     if (this.displayForm.value.wfFinalApprover4Approval != "Approved" && this.displayForm.value.wfFinalApprover3Approval == "Approved") {

                //         if (_actamount > 500000) {
                //             let x = this.userEmpList.filter(x => x.email == this.authService.email)
                //             if (x.length > 0) {
                //                 if (x[0].name == this.displayForm.value.wfFinalApprover4)
                //                     this.showFA4btn = true;
                //             }
                //         }
                //     }

                // }
                else if (this.displayForm.value.wfCurrentApprover == "FinalApprover5" || this.displayForm.value.wfCurrentApprover == "A5") {
                    if (this.displayForm.value.wfFinalApprover5Approval != "Approved" && this.displayForm.value.wfGlobalFinanceApprover1Approval == "Approved") {

                        if (_actamount > 50000) {
                            let x = this.userEmpList.filter(x => x.email == this.authService.email)
                            if (x.length > 0) {
                                if (x[0].name == this.displayForm.value.wfFinalApprover5 || x[0].name == "Silva, Coren")
                                    this.showFA5btn = true;
                            }
                        }
                    }

                }
                else if (this.displayForm.value.wfCurrentApprover == "FinalApprover6" || this.displayForm.value.wfCurrentApprover == "A6") {
                    if (this.displayForm.value.wfFinalApprover6Approval != "Approved" && this.displayForm.value.wfFinalApprover5Approval == "Approved") {

                        if (_actamount > 50000) {
                            let x = this.userEmpList.filter(x => x.email == this.authService.email)
                            if (x.length > 0) {
                                if (x[0].name == this.displayForm.value.wfFinalApprover6 || x[0].name == "Silva, Coren")
                                    this.showFA6btn = true;
                            }
                        }
                    }
                }
                if(newDate >= appDate2){
                  
                   
                    if (_actamount > 50000) {
                        if (this.displayForm.value.wfFinalApprover4 == null || this.displayForm.value.wfFinalApprover4 == "") {
                            this.showExecApp = true;
                           
                        }
                        if (this.displayForm.value.wfFinalApprover5 == null || this.displayForm.value.wfFinalApprover5 == "") {
                            this.showCFOApp = true;
                        }

                    }
                    this.showFinApp1 = false;
                    this.showGlobalFinApp1 =false;
                    this.showBU1 = false;
                    this.showBU2 = false;
                    this.showFA1 = false;
                    this.showFA2 = false;
                    this.showFA3 = false;
                    this.showFA4 = false;
                    this.showFA5 = false;
                    this.showFA6 = false;
                }
                else if (newDate >= appDate) {
                  
                    if (_actamount > 50000) {
                        if (this.displayForm.value.wfFinanceApprover1 == null || this.displayForm.value.wfFinanceApprover1 == "") {
                            this.showGlobalFinApp1 = true;
                        }
                    }
                    if (_actamount > 100000) {

                        if (this.displayForm.value.wfGlobalFinanceApprover1 == null || this.displayForm.value.wfGlobalFinanceApprover1 == "") {
                            this.showFA1 = true;
                        }
                    }
                    if (_actamount > 250000) {
                        if (this.displayForm.value.wfFinalApprover1 == null || this.displayForm.value.wfFinalApprover1 == "") {
                            this.showFA2 = true;
                        }
                    }
                    if (_actamount > 500000) {
                        if (this.displayForm.value.wfFinalApprover2 == null || this.displayForm.value.wfFinalApprover2 == "") {
                            this.showFA3 = true;
                        }
                        else if (this.displayForm.value.wfFinalApprover3 == null || this.displayForm.value.wfFinalApprover3 == ""){
                            this.showFA4 = true;
                        }
                    }
                    if (_actamount > 2000000) {
                       
                        if (this.displayForm.value.wfFinalApprover4 == null || this.displayForm.value.wfFinalApprover4 == "") {
                            this.showFA5 = true;
                        }

                    }
                    if (_actamount >  3000000) {

                        if (this.displayForm.value.wfFinalApprover5 == null || this.displayForm.value.wfFinalApprover5 == "") {
                            this.showFA6 = true;
                        }
                    }
                }


                if (this.displayForm.value.wfApprover7ApprovalDetails == null || this.displayForm.value.wfApprover7ApprovalDetails == "")
                    this.showFA7 = false;




            });
    }

    onCancel() {
        this.router.navigateByUrl('/view');
    }
    onResend() {
        console.log(this.displayForm.value);
        this.itemService.sendNotification(this.displayForm.value.id, this.displayForm.value);
    }
    showApproval(id): void {
        this.router.navigate(['/approval/', id]);
    }

    downloadAttachment(_filename: string, _folderID: string): void {
        this.itemService.downloadAttachment(_filename, _folderID)
            .subscribe(x => {
                // It is necessary to create a new blob object with mime-type explicitly set
                // otherwise only Chrome works like it should
                var newBlob = new Blob([x], { type: 'application/octet-stream' });

                // IE doesn't allow using a blob object directly as link href
                // instead it is necessary to use msSaveOrOpenBlob
                if (window.navigator && window.navigator.msSaveOrOpenBlob) {
                    window.navigator.msSaveOrOpenBlob(newBlob);
                    return;
                }

                // For other browsers: 
                // Create a link pointing to the ObjectURL containing the blob.
                const data = window.URL.createObjectURL(newBlob);

                var link = document.createElement('a');
                link.href = data;
                link.href = data;
                link.download = _filename;
                // this is necessary as link.click() does not work on the latest firefox
                link.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true, view: window }));

            });
    }

}