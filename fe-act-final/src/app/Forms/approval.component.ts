import { OnInit, Component } from "@angular/core";
import { FormBuilder, FormGroup } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { AuthService } from "../services/auth.service";
import { itemService } from '../items/item.service';
import { ApproverDetail } from "../items/item";

@Component({
    selector: 'approval-form',
    templateUrl: './approval.component.html'

})

export class ApprovalComponent implements OnInit {

    heroForm: FormGroup;
    approvalDetail: ApproverDetail[] = [];
    approvedRequest: boolean = false;
    submitApproval: boolean = false;
    validApproval: boolean = false;
    showInformation: boolean = false;
    information: string;
    currentStringDate: string;

    constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private fb: FormBuilder,
        private authService: AuthService,
        private itemService: itemService) {

    }


    ngOnInit() {

        const param = this.route.snapshot.paramMap.get('id');
        if (param) {
            const id = +param;
            this.heroForm = this.formBuilder.group({
                id: [id],
                tempstatus: [''],
                comment: [''],
                projectName: [''],
                projectManagerName: [''],
                classification: [''],
                projectDescription: [''],
                startContruction: [''],
                startUpDate: [''],
                economicLife: [''],
                actAmount: [''],
                location: [''],
                region: [''],
                entity: [''],
                status: [''],
                createdBy: [''],
                createdByEmail: [''],
                created: [''],
                doesNeedEHSReview: [''],
                wfCurrentApprover: [''],
                wfCurrentApproverEmail: [''],
                wfCurrentApproverName: [''],
                wfehsApprover: [''],
                wfehsApproverApproval: [''],
                wfehsApproverComment: [''],
                wfehsReviewer: [''],
                wfehsReviewerApproval: [''],
                wfehsReviewerComment: [''],
                wfspReviewer: [''],
                wfspReviewerApproval: [''],
                wfspReviewerComment: [''],
                wfbU1Approver: [''],
                wfbU1ApproverApproval: [''],
                wfbU1ApproverComment: [''],
                wfbU2Approver: [''],
                wfbU2ApproverApproval: [''],
                wfbU2ApproverComment: [''],
                wfFinanceApprover1: [''],
                wfFinanceApprover1Approval: [''],
                wfFinanceApprover1Comment: [''],
                wfGlobalFinanceApprover1: [''],
                wfGlobalFinanceApprover1Approval: [''],
                wfGlobalFinanceApprover1Comment: [''],
                wfFinalApprover1: [''],
                wfFinalApprover1Approval: [''],
                wfFinalApprover1Comment: [''],
                wfFinalApprover2: [''],
                wfFinalApprover2Approval: [''],
                wfFinalApprover2Comment: [''],
                wfFinalApprover3: [''],
                wfFinalApprover3Approval: [''],
                wfFinalApprover3Comment: [''],
                wfFinalApprover4: [''],
                wfFinalApprover4Approval: [''],
                wfFinalApprover4Comment: [''],
                wfFinalApprover5: [''],
                wfFinalApprover5Approval: [''],
                wfFinalApprover5Comment: [''],
                wfFinalApprover6: [''],
                wfFinalApprover6Approval: [''],
                wfFinalApprover6Comment: [''],
                lastApprovedDate: ['']
            });

            this.getApprovalItem(id.toString());
        }

    }


    getApprovalItem(reqid: string) {
        this.itemService.getACTs(reqid).subscribe(
            res => {
                this.heroForm.patchValue(res);
                console.log(this.heroForm.value);
            }
        );
    }


    OnSubmit(): void {
        this.currentStringDate = new Date().toISOString().substring(0, 10);
        this.heroForm.controls['lastApprovedDate'].setValue(this.currentStringDate);

        let newDate = new Date(this.heroForm.value.created);
        let appDate = new Date('09-16-2020');
        var _actamt = this.heroForm.value.actAmount;
        this.showInformation = true;
        this.information = "You have successfully submitted the request";

        if (this.heroForm.value.doesNeedEHSReview == false) {
            this.heroForm.controls['wfehsApproverApproval'].setValue("Approved");
            this.heroForm.controls['wfehsApproverComment'].setValue("No Need for EHS Approver");
            this.heroForm.controls['wfehsReviewerApproval'].setValue("Approved");
            this.heroForm.controls['wfehsReviewerComment'].setValue("No Need for EHS Review");
            if (this.heroForm.value.wfCurrentApprover == "EHSApprover") {
                this.heroForm.controls['wfCurrentApprover'].setValue("S&PReviewer");
                this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfspReviewer);
            }
        }

        if (this.heroForm.value.wfCurrentApprover == "EHSApprover") {
            if (newDate > appDate) {
                if (this.heroForm.value.wfehsApprover != null || this.heroForm.value.wfehsApprover != "") {
                    this.heroForm.controls['wfehsApproverApproval'].setValue(this.heroForm.value.tempstatus);
                    this.heroForm.controls['wfehsApproverComment'].setValue(this.heroForm.value.comment);
                    if (this.heroForm.value.tempstatus == "Reject") {
                        this.heroForm.controls['status'].setValue("Reject");
                        this.heroForm.controls['wfCurrentApprover'].setValue("Done");
                    }
                    else if (this.heroForm.value.tempstatus == "Approved")
                        this.heroForm.controls['status'].setValue("Pending");
                }
                else {
                    this.heroForm.controls['wfehsApproverApproval'].setValue("Approved");
                    this.heroForm.controls['wfehsApproverComment'].setValue("No EHS Approver, default to approved");
                    this.heroForm.controls['status'].setValue("Pending");

                }
                if (this.heroForm.value.status == 'Pending') {
                    this.heroForm.controls['wfCurrentApprover'].setValue("EHSReviewer");
                    this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfehsReviewer);

                    if (this.heroForm.value.wfehsReviewer == null || this.heroForm.value.wfehsReviewer == "") {
                        this.heroForm.controls['wfCurrentApprover'].setValue("S&PReviewer");
                        this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfspReviewer);
                        this.heroForm.controls['wfehsReviewerApproval'].setValue("Approved");
                        this.heroForm.controls['wfehsReviewerComment'].setValue("No EHS Reviewer, default to approved");
                        this.heroForm.controls['status'].setValue("Pending");

                    }
                    if (this.heroForm.value.wfspReviewer == null || this.heroForm.value.wfspReviewer == "") {
                        if (this.heroForm.value.wfehsReviewerApproval == "Approved") {
                            this.heroForm.controls['wfCurrentApprover'].setValue("BU1Approver");
                            this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfbU1Approver);
                            this.heroForm.controls['wfspReviewerApproval'].setValue("Approved");
                            this.heroForm.controls['wfspReviewerComment'].setValue("No SP Reviewer, default to approved");
                            this.heroForm.controls['status'].setValue("Pending");
                        }
                    }
                    if (this.heroForm.value.wfbU1Approver == null || this.heroForm.value.wfbU1Approver == "") {

                        if (this.heroForm.value.wfspReviewerApproval == "Approved") {
                            this.heroForm.controls['wfCurrentApprover'].setValue("FinanceApprover1");
                            this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfFinanceApprover1);
                        }

                        this.heroForm.controls['wfbU1ApproverApproval'].setValue("Approved");
                        this.heroForm.controls['wfbU1ApproverComment'].setValue("No Business Unit Approver 1, default to approved");
                        this.heroForm.controls['status'].setValue("Pending");
                    }
                    if (this.heroForm.value.wfFinanceApprover1 == null || this.heroForm.value.wfFinanceApprover1 == "") {

                        if (this.heroForm.value.wfbU1ApproverApproval == "Approved") {
                            this.heroForm.controls['wfCurrentApprover'].setValue("BU2Approver");
                            this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfbU2Approver);
                        }

                        this.heroForm.controls['wfFinanceApprover1Approval'].setValue("Approved");
                        this.heroForm.controls['wfFinanceApprover1Comment'].setValue("No Finance Approver 1, default to approved");
                        this.heroForm.controls['status'].setValue("Pending");
                    }
                    if (this.heroForm.value.wfbU2Approver == null || this.heroForm.value.wfbU2Approver == "") {

                        if (this.heroForm.value.wfFinanceApprover1Approval == "Approved") {
                            this.heroForm.controls['wfCurrentApprover'].setValue("GlobalFinanceApprover1");
                            this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfGlobalFinanceApprover1);

                        }

                        this.heroForm.controls['wfbU2ApproverApproval'].setValue("Approved");
                        this.heroForm.controls['wfbU2ApproverComment'].setValue("No Business Unit Approver 2, default to approved");
                        this.heroForm.controls['status'].setValue("Pending");
                    }
                    if (this.heroForm.value.wfGlobalFinanceApprover1 == null || this.heroForm.value.wfGlobalFinanceApprover1 == "") {

                        if (this.heroForm.value.wfbU2ApproverApproval == "Approved") {
                            this.heroForm.controls['wfCurrentApprover'].setValue("FinalApprover5");
                            this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfFinalApprover5);

                        }


                        this.heroForm.controls['wfGlobalFinanceApprover1Approval'].setValue("Approved");
                        this.heroForm.controls['wfGlobalFinanceApprover1Comment'].setValue("No Global Finance Approver 1, default to approved");
                        this.heroForm.controls['status'].setValue("Pending");
                    }

                    if (this.heroForm.value.wfFinalApprover5 == null || this.heroForm.value.wfFinalApprover5 == "") {
                        if (this.heroForm.value.wfGlobalFinanceApprover1Approval == "Approved") {
                            this.heroForm.controls['wfCurrentApprover'].setValue("FinalApprover6");
                            this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfFinalApprover6);

                        }

                        this.heroForm.controls['wfFinalApprover5Approval'].setValue("Approved");
                        this.heroForm.controls['wfFinalApprover5Comment'].setValue("No Final Approver 5, default to approved");
                        this.heroForm.controls['status'].setValue("Pending");
                    }
                    if (this.heroForm.value.wfFinalApprover6 == null || this.heroForm.value.wfFinalApprover6 == "") {
                        if (this.heroForm.value.wfFinalApprover5Approval == "Approved") {
                            this.heroForm.controls['wfCurrentApprover'].setValue("Done");
                            this.heroForm.controls['wfFinalApprover6Approval'].setValue("Approved");
                            this.heroForm.controls['wfFinalApprover6Comment'].setValue("No Final Approver 6, default to approved");
                            this.heroForm.controls['status'].setValue("Approved");
                        }
                    }
                }
            }
            else if (newDate < appDate) {
                if (this.heroForm.value.wfehsApprover != null || this.heroForm.value.wfehsApprover != "") {
                    this.heroForm.controls['wfehsApproverApproval'].setValue(this.heroForm.value.tempstatus);
                    this.heroForm.controls['wfehsApproverComment'].setValue(this.heroForm.value.comment);
                    if (this.heroForm.value.tempstatus == "Reject") {
                        this.heroForm.controls['status'].setValue("Reject");
                        this.heroForm.controls['wfCurrentApprover'].setValue("Done");
                    }
                    else if (this.heroForm.value.tempstatus == "Approved")
                        this.heroForm.controls['status'].setValue("Pending");
                }
                else {
                    this.heroForm.controls['wfehsApproverApproval'].setValue("Approved");
                    this.heroForm.controls['wfehsApproverComment'].setValue("No EHS Approver, default to approved");
                    this.heroForm.controls['status'].setValue("Pending");

                }
                if (this.heroForm.value.status == 'Pending') {
                    this.heroForm.controls['wfCurrentApprover'].setValue("EHSReviewer");
                    this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfehsReviewer);

                    if (this.heroForm.value.wfehsReviewer == null || this.heroForm.value.wfehsReviewer == "") {
                        this.heroForm.controls['wfCurrentApprover'].setValue("S&PReviewer");
                        this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfspReviewer);
                        this.heroForm.controls['wfehsReviewerApproval'].setValue("Approved");
                        this.heroForm.controls['wfehsReviewerComment'].setValue("No EHS Reviewer, default to approved");
                        this.heroForm.controls['status'].setValue("Pending");

                    }
                    if (this.heroForm.value.wfspReviewer == null || this.heroForm.value.wfspReviewer == "") {
                        if (this.heroForm.value.wfehsReviewerApproval == "Approved") {
                            this.heroForm.controls['wfCurrentApprover'].setValue("BU1Approver");
                            this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfbU1Approver);
                            this.heroForm.controls['wfspReviewerApproval'].setValue("Approved");
                            this.heroForm.controls['wfspReviewerComment'].setValue("No SP Reviewer, default to approved");
                            this.heroForm.controls['status'].setValue("Pending");
                        }
                    }
                    if (this.heroForm.value.wfbU1Approver == null || this.heroForm.value.wfbU1Approver == "") {

                        if (this.heroForm.value.wfspReviewerApproval == "Approved") {
                            this.heroForm.controls['wfCurrentApprover'].setValue("BU2Approver");
                            this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfbU2Approver);
                        }

                        this.heroForm.controls['wfbU1ApproverApproval'].setValue("Approved");
                        this.heroForm.controls['wfbU1ApproverComment'].setValue("No Business Unit Approver 1, default to approved");
                        this.heroForm.controls['status'].setValue("Pending");
                    }
                    if (this.heroForm.value.wfFinanceApprover1 == null || this.heroForm.value.wfFinanceApprover1 == "") {

                        if (this.heroForm.value.wfbU2ApproverApproval == "Approved") {
                            this.heroForm.controls['wfCurrentApprover'].setValue("GlobalFinanceApprover1");
                            this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfGlobalFinanceApprover1);
                        }

                        this.heroForm.controls['wfFinanceApprover1Approval'].setValue("Approved");
                        this.heroForm.controls['wfFinanceApprover1Comment'].setValue("No Finance Approver 1, default to approved");
                        this.heroForm.controls['status'].setValue("Pending");
                    }
                    if (this.heroForm.value.wfbU2Approver == null || this.heroForm.value.wfbU2Approver == "") {

                        if (this.heroForm.value.wfbU1ApproverApproval == "Approved") {
                            this.heroForm.controls['wfCurrentApprover'].setValue("FinanceApprover1");
                            this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfFinanceApprover1);
                        }

                        this.heroForm.controls['wfbU2ApproverApproval'].setValue("Approved");
                        this.heroForm.controls['wfbU2ApproverComment'].setValue("No Business Unit Approver 2, default to approved");
                        this.heroForm.controls['status'].setValue("Pending");
                    }
                    if (this.heroForm.value.wfGlobalFinanceApprover1 == null || this.heroForm.value.wfGlobalFinanceApprover1 == "") {

                        if (this.heroForm.value.wfFinanceApprover1Approval == "Approved") {
                            this.heroForm.controls['wfCurrentApprover'].setValue("FinalApprover1");
                            this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfFinalApprover1);
                        }

                        this.heroForm.controls['wfGlobalFinanceApprover1Approval'].setValue("Approved");
                        this.heroForm.controls['wfGlobalFinanceApprover1Comment'].setValue("No Global Finance Approver 1, default to approved");
                        this.heroForm.controls['status'].setValue("Pending");
                    }
                    if (this.heroForm.value.wfFinalApprover1 == null || this.heroForm.value.wfFinalApprover1 == "") {

                        if (this.heroForm.value.wfGlobalFinanceApprover1Approval == "Approved") {
                            this.heroForm.controls['wfCurrentApprover'].setValue("FinalApprover2");
                            this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfFinalApprover2);
                            this.heroForm.controls['wfFinalApprover1Approval'].setValue("Approved");
                            this.heroForm.controls['wfFinalApprover1Comment'].setValue("No Final Approver 1, default to approved");
                            this.heroForm.controls['status'].setValue("Pending");
                        }

                    }
                    if (this.heroForm.value.wfFinalApprover2 == null || this.heroForm.value.wfFinalApprover2 == "") {

                        if (this.heroForm.value.wfFinalApprover1Approval == "Approved") {
                            this.heroForm.controls['wfCurrentApprover'].setValue("FinalApprover3");
                            this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfFinalApprover3);
                            this.heroForm.controls['wfFinalApprover2Approval'].setValue("Approved");
                            this.heroForm.controls['wfFinalApprover2Comment'].setValue("No Final Approver 2, default to approved");
                            this.heroForm.controls['status'].setValue("Pending");
                        }

                    }
                    if (this.heroForm.value.wfFinalApprover3 == null || this.heroForm.value.wfFinalApprover3 == "") {

                        if (this.heroForm.value.wfFinalApprover2Approval == "Approved") {
                            this.heroForm.controls['wfCurrentApprover'].setValue("FinalApprover4");
                            this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfFinalApprover4);
                            this.heroForm.controls['wfFinalApprover3Approval'].setValue("Approved");
                            this.heroForm.controls['wfFinalApprover3Comment'].setValue("No Final Approver 3, default to approved");
                            this.heroForm.controls['status'].setValue("Pending");
                        }

                    }
                    if (this.heroForm.value.wfFinalApprover4 == null || this.heroForm.value.wfFinalApprover4 == "") {

                        if (this.heroForm.value.wfFinalApprover3Approval == "Approved") {
                            this.heroForm.controls['wfCurrentApprover'].setValue("FinalApprover5");
                            this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfFinalApprover5);
                            this.heroForm.controls['wfFinalApprover4Approval'].setValue("Approved");
                            this.heroForm.controls['wfFinalApprover4Comment'].setValue("No Final Approver 4, default to approved");
                            this.heroForm.controls['status'].setValue("Pending");
                        }

                    }


                    if (this.heroForm.value.wfFinalApprover5 == null || this.heroForm.value.wfFinalApprover5 == "") {

                        if (this.heroForm.value.wfFinalApprover4Approval == "Approved") {
                            this.heroForm.controls['wfCurrentApprover'].setValue("FinalApprover6");
                            this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfFinalApprover6);
                        }

                        this.heroForm.controls['wfFinalApprover5Approval'].setValue("Approved");
                        this.heroForm.controls['wfFinalApprover5Comment'].setValue("No Final Approver 5, default to approved");
                        this.heroForm.controls['status'].setValue("Pending");
                    }
                    if (this.heroForm.value.wfFinalApprover6 == null || this.heroForm.value.wfFinalApprover6 == "") {
                        if (this.heroForm.value.wfFinalApprover5Approval == "Approved") {
                            this.heroForm.controls['wfCurrentApprover'].setValue("Done");
                            this.heroForm.controls['wfFinalApprover6Approval'].setValue("Approved");
                            this.heroForm.controls['wfFinalApprover6Comment'].setValue("No Final Approver 6, default to approved");
                            this.heroForm.controls['status'].setValue("Approved");
                        }
                    }
                }
            }
        }
        else if (this.heroForm.value.wfCurrentApprover == "EHSReviewer") {
            if (newDate > appDate) {
                if (this.heroForm.value.wfehsApproverApproval == "Approved" && this.heroForm.value.wfehsReviewerApproval == "") {

                    this.heroForm.controls['wfehsReviewerApproval'].setValue(this.heroForm.value.tempstatus);
                    this.heroForm.controls['wfehsReviewerComment'].setValue(this.heroForm.value.comment);
                    if (this.heroForm.value.tempstatus == "Reject")
                        this.heroForm.controls['status'].setValue("Reject");
                    else if (this.heroForm.value.tempstatus == "Approved")
                        this.heroForm.controls['status'].setValue("Pending");

                    if (this.heroForm.value.status == 'Pending') {
                        this.heroForm.controls['wfCurrentApprover'].setValue("S&PReviewer");
                        this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfspReviewer);

                        if (this.heroForm.value.wfspReviewer == null || this.heroForm.value.wfspReviewer == "") {
                            if (this.heroForm.value.wfehsReviewerApproval == "Approved") {
                                this.heroForm.controls['wfCurrentApprover'].setValue("BU1Approver");
                                this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfbU1Approver);
                                this.heroForm.controls['wfspReviewerApproval'].setValue("Approved");
                                this.heroForm.controls['wfspReviewerComment'].setValue("No SP Reviewer, default to approved");
                                this.heroForm.controls['status'].setValue("Pending");
                            }
                        }
                        if (this.heroForm.value.wfbU1Approver == null || this.heroForm.value.wfbU1Approver == "") {

                            if (this.heroForm.value.wfspReviewerApproval == "Approved") {
                                this.heroForm.controls['wfCurrentApprover'].setValue("FinanceApprover1");
                                this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfFinanceApprover1);
                            }

                            this.heroForm.controls['wfbU1ApproverApproval'].setValue("Approved");
                            this.heroForm.controls['wfbU1ApproverComment'].setValue("No Business Unit Approver 1, default to approved");
                            this.heroForm.controls['status'].setValue("Pending");
                        }
                        if (this.heroForm.value.wfFinanceApprover1 == null || this.heroForm.value.wfFinanceApprover1 == "") {

                            if (this.heroForm.value.wfbU1ApproverApproval == "Approved") {
                                this.heroForm.controls['wfCurrentApprover'].setValue("BU2Approver");
                                this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfbU2Approver);
                            }

                            this.heroForm.controls['wfFinanceApprover1Approval'].setValue("Approved");
                            this.heroForm.controls['wfFinanceApprover1Comment'].setValue("No Finance Approver 1, default to approved");
                            this.heroForm.controls['status'].setValue("Pending");
                        }
                        if (this.heroForm.value.wfbU2Approver == null || this.heroForm.value.wfbU2Approver == "") {

                            if (this.heroForm.value.wfFinanceApprover1Approval == "Approved") {
                                this.heroForm.controls['wfCurrentApprover'].setValue("GlobalFinanceApprover1");
                                this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfGlobalFinanceApprover1);

                            }

                            this.heroForm.controls['wfbU2ApproverApproval'].setValue("Approved");
                            this.heroForm.controls['wfbU2ApproverComment'].setValue("No Business Unit Approver 2, default to approved");
                            this.heroForm.controls['status'].setValue("Pending");
                        }
                        if (this.heroForm.value.wfGlobalFinanceApprover1 == null || this.heroForm.value.wfGlobalFinanceApprover1 == "") {

                            if (this.heroForm.value.wfbU2ApproverApproval == "Approved") {
                                this.heroForm.controls['wfCurrentApprover'].setValue("FinalApprover5");
                                this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfFinalApprover5);

                            }

                            this.heroForm.controls['wfGlobalFinanceApprover1Approval'].setValue("Approved");
                            this.heroForm.controls['wfGlobalFinanceApprover1Comment'].setValue("No Global Finance Approver 1, default to approved");
                            this.heroForm.controls['status'].setValue("Pending");
                        }

                        if (this.heroForm.value.wfFinalApprover5 == null || this.heroForm.value.wfFinalApprover5 == "") {

                            if (this.heroForm.value.wfGlobalFinanceApprover1Approval == "Approved") {
                                this.heroForm.controls['wfCurrentApprover'].setValue("FinalApprover6");
                                this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfFinalApprover6);

                            }

                            this.heroForm.controls['wfFinalApprover5Approval'].setValue("Approved");
                            this.heroForm.controls['wfFinalApprover5Comment'].setValue("No Final Approver 5, default to approved");
                            this.heroForm.controls['status'].setValue("Pending");
                        }
                        if (this.heroForm.value.wfFinalApprover6 == null || this.heroForm.value.wfFinalApprover6 == "") {
                            if (this.heroForm.value.wfFinalApprover5Approval == "Approved") {
                                this.heroForm.controls['wfCurrentApprover'].setValue("Done");
                                this.heroForm.controls['wfFinalApprover6Approval'].setValue("Approved");
                                this.heroForm.controls['wfFinalApprover6Comment'].setValue("No Final Approver 6, default to approved");
                                this.heroForm.controls['status'].setValue("Approved");
                            }
                        }
                    }
                }
            }
            else if (newDate < appDate) {
                if (this.heroForm.value.wfehsApproverApproval == "Approved" && this.heroForm.value.wfehsReviewerApproval == "") {

                    this.heroForm.controls['wfehsReviewerApproval'].setValue(this.heroForm.value.tempstatus);
                    this.heroForm.controls['wfehsReviewerComment'].setValue(this.heroForm.value.comment);
                    if (this.heroForm.value.tempstatus == "Reject")
                        this.heroForm.controls['status'].setValue("Reject");
                    else if (this.heroForm.value.tempstatus == "Approved")
                        this.heroForm.controls['status'].setValue("Pending");

                    if (this.heroForm.value.status == 'Pending') {
                        this.heroForm.controls['wfCurrentApprover'].setValue("S&PReviewer");
                        this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfspReviewer);

                        if (this.heroForm.value.wfspReviewer == null || this.heroForm.value.wfspReviewer == "") {
                            if (this.heroForm.value.wfehsReviewerApproval == "Approved") {
                                this.heroForm.controls['wfCurrentApprover'].setValue("BU1Approver");
                                this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfbU1Approver);
                                this.heroForm.controls['wfspReviewerApproval'].setValue("Approved");
                                this.heroForm.controls['wfspReviewerComment'].setValue("No SP Reviewer, default to approved");
                                this.heroForm.controls['status'].setValue("Pending");
                            }
                        }
                        if (this.heroForm.value.wfbU1Approver == null || this.heroForm.value.wfbU1Approver == "") {

                            if (this.heroForm.value.wfspReviewerApproval == "Approved") {
                                this.heroForm.controls['wfCurrentApprover'].setValue("BU2Approver");
                                this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfbU2Approver);
                            }

                            this.heroForm.controls['wfbU1ApproverApproval'].setValue("Approved");
                            this.heroForm.controls['wfbU1ApproverComment'].setValue("No Business Unit Approver 1, default to approved");
                            this.heroForm.controls['status'].setValue("Pending");
                        }
                        if (this.heroForm.value.wfFinanceApprover1 == null || this.heroForm.value.wfFinanceApprover1 == "") {

                            if (this.heroForm.value.wfbU2ApproverApproval == "Approved") {
                                this.heroForm.controls['wfCurrentApprover'].setValue("GlobalFinanceApprover1");
                                this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfGlobalFinanceApprover1);
                            }
                            this.heroForm.controls['wfFinanceApprover1Approval'].setValue("Approved");
                            this.heroForm.controls['wfFinanceApprover1Comment'].setValue("No Finance Approver 1, default to approved");
                            this.heroForm.controls['status'].setValue("Pending");
                        }
                        if (this.heroForm.value.wfbU2Approver == null || this.heroForm.value.wfbU2Approver == "") {

                            if (this.heroForm.value.wfbU1ApproverApproval == "Approved") {
                                this.heroForm.controls['wfCurrentApprover'].setValue("FinanceApprover1");
                                this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfFinanceApprover1);
                            }
                            this.heroForm.controls['wfbU2ApproverApproval'].setValue("Approved");
                            this.heroForm.controls['wfbU2ApproverComment'].setValue("No Business Unit Approver 2, default to approved");
                            this.heroForm.controls['status'].setValue("Pending");
                        }
                        if (this.heroForm.value.wfGlobalFinanceApprover1 == null || this.heroForm.value.wfGlobalFinanceApprover1 == "") {

                            if (this.heroForm.value.wfFinanceApprover1Approval == "Approved") {
                                this.heroForm.controls['wfCurrentApprover'].setValue("FinalApprover1");
                                this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfFinalApprover1);
                            }
                            this.heroForm.controls['wfGlobalFinanceApprover1Approval'].setValue("Approved");
                            this.heroForm.controls['wfGlobalFinanceApprover1Comment'].setValue("No Global Finance Approver 1, default to approved");
                            this.heroForm.controls['status'].setValue("Pending");
                        }
                        if (this.heroForm.value.wfFinalApprover1 == null || this.heroForm.value.wfFinalApprover1 == "") {

                            if (this.heroForm.value.wfGlobalFinanceApprover1Approval == "Approved") {
                                this.heroForm.controls['wfCurrentApprover'].setValue("FinalApprover2");
                                this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfFinalApprover2);
                                this.heroForm.controls['wfFinalApprover1Approval'].setValue("Approved");
                                this.heroForm.controls['wfFinalApprover1Comment'].setValue("No Final Approver 1, default to approved");
                                this.heroForm.controls['status'].setValue("Pending");
                            }

                        }
                        if (this.heroForm.value.wfFinalApprover2 == null || this.heroForm.value.wfFinalApprover2 == "") {

                            if (this.heroForm.value.wfFinalApprover1Approval == "Approved") {
                                this.heroForm.controls['wfCurrentApprover'].setValue("FinalApprover3");
                                this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfFinalApprover3);
                                this.heroForm.controls['wfFinalApprover2Approval'].setValue("Approved");
                                this.heroForm.controls['wfFinalApprover2Comment'].setValue("No Final Approver 2, default to approved");
                                this.heroForm.controls['status'].setValue("Pending");
                            }

                        }
                        if (this.heroForm.value.wfFinalApprover3 == null || this.heroForm.value.wfFinalApprover3 == "") {

                            if (this.heroForm.value.wfFinalApprover2Approval == "Approved") {
                                this.heroForm.controls['wfCurrentApprover'].setValue("FinalApprover4");
                                this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfFinalApprover4);
                                this.heroForm.controls['wfFinalApprover3Approval'].setValue("Approved");
                                this.heroForm.controls['wfFinalApprover3Comment'].setValue("No Final Approver 3, default to approved");
                                this.heroForm.controls['status'].setValue("Pending");
                            }

                        }
                        if (this.heroForm.value.wfFinalApprover4 == null || this.heroForm.value.wfFinalApprover4 == "") {

                            if (this.heroForm.value.wfFinalApprover3Approval == "Approved") {
                                this.heroForm.controls['wfCurrentApprover'].setValue("FinalApprover5");
                                this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfFinalApprover5);
                                this.heroForm.controls['wfFinalApprover4Approval'].setValue("Approved");
                                this.heroForm.controls['wfFinalApprover4Comment'].setValue("No Final Approver 4, default to approved");
                                this.heroForm.controls['status'].setValue("Pending");
                            }

                        }


                        if (this.heroForm.value.wfFinalApprover5 == null || this.heroForm.value.wfFinalApprover5 == "") {

                            if (this.heroForm.value.wfFinalApprover4Approval == "Approved") {
                                this.heroForm.controls['wfCurrentApprover'].setValue("FinalApprover6");
                                this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfFinalApprover6);
                            }

                            this.heroForm.controls['wfFinalApprover5Approval'].setValue("Approved");
                            this.heroForm.controls['wfFinalApprover5Comment'].setValue("No Final Approver 5, default to approved");
                            this.heroForm.controls['status'].setValue("Pending");
                        }
                        if (this.heroForm.value.wfFinalApprover6 == null || this.heroForm.value.wfFinalApprover6 == "") {
                            if (this.heroForm.value.wfFinalApprover5Approval == "Approved") {
                                this.heroForm.controls['wfCurrentApprover'].setValue("Done");
                                this.heroForm.controls['wfFinalApprover6Approval'].setValue("Approved");
                                this.heroForm.controls['wfFinalApprover6Comment'].setValue("No Final Approver 6, default to approved");
                                this.heroForm.controls['status'].setValue("Approved");
                            }
                        }
                    }
                }
            }

        }
        else if (this.heroForm.value.wfCurrentApprover == "S&PReviewer") {
            if (newDate > appDate) {
                if (this.heroForm.value.wfehsReviewerApproval == "Approved" && this.heroForm.value.wfspReviewerApproval == "") {
                    this.heroForm.controls['wfspReviewerApproval'].setValue(this.heroForm.value.tempstatus);
                    this.heroForm.controls['wfspReviewerComment'].setValue(this.heroForm.value.comment);
                    if (this.heroForm.value.tempstatus == "Reject")
                        this.heroForm.controls['status'].setValue("Reject");

                    else if (this.heroForm.value.tempstatus == "Approved") {
                        this.heroForm.controls['status'].setValue("Pending");

                        if (this.heroForm.value.status == 'Pending') {
                            this.heroForm.controls['wfCurrentApprover'].setValue("BU1Approver");
                            this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfbU1Approver);

                            if (this.heroForm.value.wfbU1Approver == null || this.heroForm.value.wfbU1Approver == "") {
                                if (this.heroForm.value.wfspReviewerApproval == "Approved") {
                                    this.heroForm.controls['wfCurrentApprover'].setValue("FinanceApprover1");
                                    this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfFinanceApprover1);
                                }
                                this.heroForm.controls['wfbU1ApproverApproval'].setValue("Approved");
                                this.heroForm.controls['wfbU1ApproverComment'].setValue("No Business Unit Approver 1, default to approved");
                                this.heroForm.controls['status'].setValue("Pending");
                            }
                            if (this.heroForm.value.wfFinanceApprover1 == null || this.heroForm.value.wfFinanceApprover1 == "") {

                                if (this.heroForm.value.wfbU1ApproverApproval == "Approved") {
                                    this.heroForm.controls['wfCurrentApprover'].setValue("BU2Approver");
                                    this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfbU2Approver);
                                }

                                this.heroForm.controls['wfFinanceApprover1Approval'].setValue("Approved");
                                this.heroForm.controls['wfFinanceApprover1Comment'].setValue("No Finance Approver 1, default to approved");
                                this.heroForm.controls['status'].setValue("Pending");
                            }
                            if (this.heroForm.value.wfbU2Approver == null || this.heroForm.value.wfbU2Approver == "") {

                                if (this.heroForm.value.wfFinanceApprover1Approval == "Approved") {
                                    this.heroForm.controls['wfCurrentApprover'].setValue("GlobalFinanceApprover1");
                                    this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfGlobalFinanceApprover1);

                                }

                                this.heroForm.controls['wfbU2ApproverApproval'].setValue("Approved");
                                this.heroForm.controls['wfbU2ApproverComment'].setValue("No Business Unit Approver 2, default to approved");
                                this.heroForm.controls['status'].setValue("Pending");
                            }
                            if (this.heroForm.value.wfGlobalFinanceApprover1 == null || this.heroForm.value.wfGlobalFinanceApprover1 == "") {

                                if (this.heroForm.value.wfbU2ApproverApproval == "Approved") {
                                    this.heroForm.controls['wfCurrentApprover'].setValue("FinalApprover5");
                                    this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfFinalApprover5);

                                }

                                this.heroForm.controls['wfGlobalFinanceApprover1Approval'].setValue("Approved");
                                this.heroForm.controls['wfGlobalFinanceApprover1Comment'].setValue("No Global Finance Approver 1, default to approved");
                                this.heroForm.controls['status'].setValue("Pending");
                            }

                            if (this.heroForm.value.wfFinalApprover5 == null || this.heroForm.value.wfFinalApprover5 == "") {

                                if (this.heroForm.value.wfGlobalFinanceApprover1Approval == "Approved") {
                                    this.heroForm.controls['wfCurrentApprover'].setValue("FinalApprover6");
                                    this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfFinalApprover6);

                                }

                                this.heroForm.controls['wfFinalApprover5Approval'].setValue("Approved");
                                this.heroForm.controls['wfFinalApprover5Comment'].setValue("No Final Approver 5, default to approved");
                                this.heroForm.controls['status'].setValue("Pending");
                            }
                            if (this.heroForm.value.wfFinalApprover6 == null || this.heroForm.value.wfFinalApprover6 == "") {
                                if (this.heroForm.value.wfFinalApprover5Approval == "Approved") {
                                    this.heroForm.controls['wfCurrentApprover'].setValue("Done");
                                    this.heroForm.controls['wfFinalApprover6Approval'].setValue("Approved");
                                    this.heroForm.controls['wfFinalApprover6Comment'].setValue("No Final Approver 6, default to approved");
                                    this.heroForm.controls['status'].setValue("Approved");
                                }
                            }
                        }
                    }
                }
            }
            else if (newDate < appDate) {
                if (this.heroForm.value.wfehsReviewerApproval == "Approved" && this.heroForm.value.wfspReviewerApproval == "") {

                    this.heroForm.controls['wfspReviewerApproval'].setValue(this.heroForm.value.tempstatus);
                    this.heroForm.controls['wfspReviewerComment'].setValue(this.heroForm.value.comment);
                    if (this.heroForm.value.tempstatus == "Reject")
                        this.heroForm.controls['status'].setValue("Reject");


                    else if (this.heroForm.value.tempstatus == "Approved") {
                        this.heroForm.controls['status'].setValue("Pending");

                        if (this.heroForm.value.status == 'Pending') {
                            this.heroForm.controls['wfCurrentApprover'].setValue("BU1Approver");
                            this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfbU1Approver);

                            if (this.heroForm.value.wfbU1Approver == null || this.heroForm.value.wfbU1Approver == "") {

                                if (this.heroForm.value.wfspReviewerApproval == "Approved") {
                                    this.heroForm.controls['wfCurrentApprover'].setValue("BU2Approver");
                                    this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfbU2Approver);
                                }
                                this.heroForm.controls['wfbU1ApproverApproval'].setValue("Approved");
                                this.heroForm.controls['wfbU1ApproverComment'].setValue("No Business Unit Approver 1, default to approved");
                                this.heroForm.controls['status'].setValue("Pending");
                            }
                            if (this.heroForm.value.wfFinanceApprover1 == null || this.heroForm.value.wfFinanceApprover1 == "") {

                                if (this.heroForm.value.wfbU2ApproverApproval == "Approved") {
                                    this.heroForm.controls['wfCurrentApprover'].setValue("GlobalFinanceApprover1");
                                    this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfGlobalFinanceApprover1);
                                }

                                this.heroForm.controls['wfFinanceApprover1Approval'].setValue("Approved");
                                this.heroForm.controls['wfFinanceApprover1Comment'].setValue("No Finance Approver 1, default to approved");
                                this.heroForm.controls['status'].setValue("Pending");
                            }
                            if (this.heroForm.value.wfbU2Approver == null || this.heroForm.value.wfbU2Approver == "") {

                                if (this.heroForm.value.wfbU1ApproverApproval == "Approved") {
                                    this.heroForm.controls['wfCurrentApprover'].setValue("FinanceApprover1");
                                    this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfFinanceApprover1);
                                }

                                this.heroForm.controls['wfbU2ApproverApproval'].setValue("Approved");
                                this.heroForm.controls['wfbU2ApproverComment'].setValue("No Business Unit Approver 2, default to approved");
                                this.heroForm.controls['status'].setValue("Pending");
                            }
                            if (this.heroForm.value.wfGlobalFinanceApprover1 == null || this.heroForm.value.wfGlobalFinanceApprover1 == "") {

                                if (this.heroForm.value.wfFinanceApprover1Approval == "Approved") {
                                    this.heroForm.controls['wfCurrentApprover'].setValue("FinalApprover1");
                                    this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfFinalApprover1);
                                }

                                this.heroForm.controls['wfGlobalFinanceApprover1Approval'].setValue("Approved");
                                this.heroForm.controls['wfGlobalFinanceApprover1Comment'].setValue("No Global Finance Approver 1, default to approved");
                                this.heroForm.controls['status'].setValue("Pending");
                            }
                            if (this.heroForm.value.wfFinalApprover1 == null || this.heroForm.value.wfFinalApprover1 == "") {
                                if (this.heroForm.value.wfGlobalFinanceApprover1Approval == "Approved") {
                                    this.heroForm.controls['wfCurrentApprover'].setValue("FinalApprover2");
                                    this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfFinalApprover2);
                                    this.heroForm.controls['wfFinalApprover1Approval'].setValue("Approved");
                                    this.heroForm.controls['wfFinalApprover1Comment'].setValue("No Final Approver 1, default to approved");
                                    this.heroForm.controls['status'].setValue("Pending");
                                }
                            }

                            if (this.heroForm.value.wfFinalApprover2 == null || this.heroForm.value.wfFinalApprover2 == "") {

                                if (this.heroForm.value.wfFinalApprover1Approval == "Approved") {
                                    this.heroForm.controls['wfCurrentApprover'].setValue("FinalApprover3");
                                    this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfFinalApprover3);
                                    this.heroForm.controls['wfFinalApprover2Approval'].setValue("Approved");
                                    this.heroForm.controls['wfFinalApprover2Comment'].setValue("No Final Approver 2, default to approved");
                                    this.heroForm.controls['status'].setValue("Pending");
                                }

                            }
                            if (this.heroForm.value.wfFinalApprover3 == null || this.heroForm.value.wfFinalApprover3 == "") {

                                if (this.heroForm.value.wfFinalApprover2Approval == "Approved") {
                                    this.heroForm.controls['wfCurrentApprover'].setValue("FinalApprover4");
                                    this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfFinalApprover4);
                                    this.heroForm.controls['wfFinalApprover3Approval'].setValue("Approved");
                                    this.heroForm.controls['wfFinalApprover3Comment'].setValue("No Final Approver 3, default to approved");
                                    this.heroForm.controls['status'].setValue("Pending");
                                }

                            }
                            if (this.heroForm.value.wfFinalApprover4 == null || this.heroForm.value.wfFinalApprover4 == "") {

                                if (this.heroForm.value.wfFinalApprover3Approval == "Approved") {
                                    this.heroForm.controls['wfCurrentApprover'].setValue("FinalApprover5");
                                    this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfFinalApprover5);
                                    this.heroForm.controls['wfFinalApprover4Approval'].setValue("Approved");
                                    this.heroForm.controls['wfFinalApprover4Comment'].setValue("No Final Approver 4, default to approved");
                                    this.heroForm.controls['status'].setValue("Pending");
                                }

                            }


                            if (this.heroForm.value.wfFinalApprover5 == null || this.heroForm.value.wfFinalApprover5 == "") {

                                if (this.heroForm.value.wfFinalApprover4Approval == "Approved") {
                                    this.heroForm.controls['wfCurrentApprover'].setValue("FinalApprover6");
                                    this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfFinalApprover6);
                                }

                                this.heroForm.controls['wfFinalApprover5Approval'].setValue("Approved");
                                this.heroForm.controls['wfFinalApprover5Comment'].setValue("No Final Approver 5, default to approved");
                                this.heroForm.controls['status'].setValue("Pending");
                            }
                            if (this.heroForm.value.wfFinalApprover6 == null || this.heroForm.value.wfFinalApprover6 == "") {
                                if (this.heroForm.value.wfFinalApprover5Approval == "Approved") {
                                    this.heroForm.controls['wfCurrentApprover'].setValue("Done");
                                    this.heroForm.controls['wfFinalApprover6Approval'].setValue("Approved");
                                    this.heroForm.controls['wfFinalApprover6Comment'].setValue("No Final Approver 6, default to approved");
                                    this.heroForm.controls['status'].setValue("Approved");
                                }
                            }
                        }
                    }
                }
            }
        }
        else if (this.heroForm.value.wfCurrentApprover == "BU1Approver") {
            if (newDate > appDate) {
                if (this.heroForm.value.wfspReviewerApproval == "Approved" && this.heroForm.value.wfbU1ApproverApproval == "") {
                    this.heroForm.controls['wfbU1ApproverApproval'].setValue(this.heroForm.value.tempstatus);
                    this.heroForm.controls['wfbU1ApproverComment'].setValue(this.heroForm.value.comment);
                    if (this.heroForm.value.tempstatus == "Reject")
                        this.heroForm.controls['status'].setValue("Reject");
                    else if (this.heroForm.value.tempstatus == "Approved")
                        this.heroForm.controls['status'].setValue("Pending");

                    if (this.heroForm.value.status == 'Pending') {
                        this.heroForm.controls['wfCurrentApprover'].setValue("FinanceApprover1");
                        this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfFinanceApprover1);

                        if (this.heroForm.value.wfFinanceApprover1 == null || this.heroForm.value.wfFinanceApprover1 == "") {
                            if (this.heroForm.value.wfbU1ApproverApproval == "Approved") {
                                this.heroForm.controls['wfCurrentApprover'].setValue("BU2Approver");
                                this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfbU2Approver);
                            }
                            this.heroForm.controls['wfFinanceApprover1Approval'].setValue("Approved");
                            this.heroForm.controls['wfFinanceApprover1Comment'].setValue("No Finance Approver 1, default to approved");
                            this.heroForm.controls['status'].setValue("Pending");
                        }
                        if (this.heroForm.value.wfbU2Approver == null || this.heroForm.value.wfbU2Approver == "") {
                            if (this.heroForm.value.wfFinanceApprover1Approval == "Approved") {
                                this.heroForm.controls['wfCurrentApprover'].setValue("GlobalFinanceApprover1");
                                this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfGlobalFinanceApprover1);
                            }
                            this.heroForm.controls['wfbU2ApproverApproval'].setValue("Approved");
                            this.heroForm.controls['wfbU2ApproverComment'].setValue("No Business Unit Approver 2, default to approved");
                            this.heroForm.controls['status'].setValue("Pending");
                        }
                        if (this.heroForm.value.wfGlobalFinanceApprover1 == null || this.heroForm.value.wfGlobalFinanceApprover1 == "") {
                            if (this.heroForm.value.wfbU2ApproverApproval == "Approved") {
                                this.heroForm.controls['wfCurrentApprover'].setValue("FinalApprover5");
                                this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfFinalApprover5);
                            }
                            this.heroForm.controls['wfGlobalFinanceApprover1Approval'].setValue("Approved");
                            this.heroForm.controls['wfGlobalFinanceApprover1Comment'].setValue("No Global Finance Approver 1, default to approved");
                            this.heroForm.controls['status'].setValue("Pending");
                        }

                        if (this.heroForm.value.wfFinalApprover5 == null || this.heroForm.value.wfFinalApprover5 == "") {
                            if (this.heroForm.value.wfGlobalFinanceApprover1Approval == "Approved") {
                                this.heroForm.controls['wfCurrentApprover'].setValue("FinalApprover6");
                                this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfFinalApprover6);
                            }
                            this.heroForm.controls['wfFinalApprover5Approval'].setValue("Approved");
                            this.heroForm.controls['wfFinalApprover5Comment'].setValue("No Final Approver 5, default to approved");
                            this.heroForm.controls['status'].setValue("Pending");
                        }
                        if (this.heroForm.value.wfFinalApprover6 == null || this.heroForm.value.wfFinalApprover6 == "") {
                            if (this.heroForm.value.wfFinalApprover5Approval == "Approved") {
                                this.heroForm.controls['wfCurrentApprover'].setValue("Done");
                                this.heroForm.controls['wfFinalApprover6Approval'].setValue("Approved");
                                this.heroForm.controls['wfFinalApprover6Comment'].setValue("No Final Approver 6, default to approved");
                                this.heroForm.controls['status'].setValue("Approved");
                            }
                        }
                    }
                }
            }
            else if (newDate < appDate) {
                if (this.heroForm.value.wfspReviewerApproval == "Approved" && this.heroForm.value.wfbU1ApproverApproval == "") {
                    this.heroForm.controls['wfbU1ApproverApproval'].setValue(this.heroForm.value.tempstatus);
                    this.heroForm.controls['wfbU1ApproverComment'].setValue(this.heroForm.value.comment);
                    if (this.heroForm.value.tempstatus == "Reject")
                        this.heroForm.controls['status'].setValue("Reject");
                    else if (this.heroForm.value.tempstatus == "Approved")
                        this.heroForm.controls['status'].setValue("Pending");

                    if (this.heroForm.value.status == 'Pending') {
                        this.heroForm.controls['wfCurrentApprover'].setValue("FinanceApprover1");
                        this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfFinanceApprover1);

                        if (this.heroForm.value.wfFinanceApprover1 == null || this.heroForm.value.wfFinanceApprover1 == "") {
                            if (this.heroForm.value.wfbU2ApproverApproval == "Approved") {
                                this.heroForm.controls['wfCurrentApprover'].setValue("GlobalFinanceApprover1");
                                this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfGlobalFinanceApprover1);
                            }
                            this.heroForm.controls['wfFinanceApprover1Approval'].setValue("Approved");
                            this.heroForm.controls['wfFinanceApprover1Comment'].setValue("No Finance Approver 1, default to approved");
                            this.heroForm.controls['status'].setValue("Pending");
                        }
                        if (this.heroForm.value.wfbU2Approver == null || this.heroForm.value.wfbU2Approver == "") {
                            if (this.heroForm.value.wfbU1ApproverApproval == "Approved") {
                                this.heroForm.controls['wfCurrentApprover'].setValue("FinanceApprover1");
                                this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfFinanceApprover1);
                            }
                            this.heroForm.controls['wfbU2ApproverApproval'].setValue("Approved");
                            this.heroForm.controls['wfbU2ApproverComment'].setValue("No Business Unit Approver 2, default to approved");
                            this.heroForm.controls['status'].setValue("Pending");
                        }
                        if (this.heroForm.value.wfGlobalFinanceApprover1 == null || this.heroForm.value.wfGlobalFinanceApprover1 == "") {
                            if (this.heroForm.value.wfFinanceApprover1Approval == "Approved") {
                                this.heroForm.controls['wfCurrentApprover'].setValue("FinalApprover1");
                                this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfFinalApprover1);
                            }
                            this.heroForm.controls['wfGlobalFinanceApprover1Approval'].setValue("Approved");
                            this.heroForm.controls['wfGlobalFinanceApprover1Comment'].setValue("No Global Finance Approver 1, default to approved");
                            this.heroForm.controls['status'].setValue("Pending");
                        }
                        if (this.heroForm.value.wfFinalApprover1 == null || this.heroForm.value.wfFinalApprover1 == "") {
                            if (this.heroForm.value.wfGlobalFinanceApprover1Approval == "Approved") {
                                this.heroForm.controls['wfCurrentApprover'].setValue("FinalApprover2");
                                this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfFinalApprover2);
                                this.heroForm.controls['wfFinalApprover1Approval'].setValue("Approved");
                                this.heroForm.controls['wfFinalApprover1Comment'].setValue("No Final Approver 1, default to approved");
                                this.heroForm.controls['status'].setValue("Pending");
                            }
                        }
                        if (this.heroForm.value.wfFinalApprover2 == null || this.heroForm.value.wfFinalApprover2 == "") {

                            if (this.heroForm.value.wfFinalApprover1Approval == "Approved") {
                                this.heroForm.controls['wfCurrentApprover'].setValue("FinalApprover3");
                                this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfFinalApprover3);
                                this.heroForm.controls['wfFinalApprover2Approval'].setValue("Approved");
                                this.heroForm.controls['wfFinalApprover2Comment'].setValue("No Final Approver 2, default to approved");
                                this.heroForm.controls['status'].setValue("Pending");
                            }
                        }
                        if (this.heroForm.value.wfFinalApprover3 == null || this.heroForm.value.wfFinalApprover3 == "") {

                            if (this.heroForm.value.wfFinalApprover2Approval == "Approved") {
                                this.heroForm.controls['wfCurrentApprover'].setValue("FinalApprover4");
                                this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfFinalApprover4);
                                this.heroForm.controls['wfFinalApprover3Approval'].setValue("Approved");
                                this.heroForm.controls['wfFinalApprover3Comment'].setValue("No Final Approver 3, default to approved");
                                this.heroForm.controls['status'].setValue("Pending");
                            }
                        }
                        if (this.heroForm.value.wfFinalApprover4 == null || this.heroForm.value.wfFinalApprover4 == "") {

                            if (this.heroForm.value.wfFinalApprover3Approval == "Approved") {
                                this.heroForm.controls['wfCurrentApprover'].setValue("FinalApprover5");
                                this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfFinalApprover5);
                                this.heroForm.controls['wfFinalApprover4Approval'].setValue("Approved");
                                this.heroForm.controls['wfFinalApprover4Comment'].setValue("No Final Approver 4, default to approved");
                                this.heroForm.controls['status'].setValue("Pending");
                            }
                        }
                        if (this.heroForm.value.wfFinalApprover5 == null || this.heroForm.value.wfFinalApprover5 == "") {

                            if (this.heroForm.value.wfFinalApprover4Approval == "Approved") {
                                this.heroForm.controls['wfCurrentApprover'].setValue("FinalApprover6");
                                this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfFinalApprover6);
                            }
                            this.heroForm.controls['wfFinalApprover5Approval'].setValue("Approved");
                            this.heroForm.controls['wfFinalApprover5Comment'].setValue("No Final Approver 5, default to approved");
                            this.heroForm.controls['status'].setValue("Pending");
                        }
                        if (this.heroForm.value.wfFinalApprover6 == null || this.heroForm.value.wfFinalApprover6 == "") {
                            if (this.heroForm.value.wfFinalApprover5Approval == "Approved") {
                                this.heroForm.controls['wfCurrentApprover'].setValue("Done");
                                this.heroForm.controls['wfFinalApprover6Approval'].setValue("Approved");
                                this.heroForm.controls['wfFinalApprover6Comment'].setValue("No Final Approver 6, default to approved");
                                this.heroForm.controls['status'].setValue("Approved");
                            }
                        }
                    }
                }
            }
        }
        else if (this.heroForm.value.wfCurrentApprover == "FinanceApprover1") {
            if (newDate > appDate) {
                if (this.heroForm.value.wfbU1ApproverApproval == "Approved" && this.heroForm.value.wfFinanceApprover1Approval == "") {
                    this.heroForm.controls['wfFinanceApprover1Approval'].setValue(this.heroForm.value.tempstatus);
                    this.heroForm.controls['wfFinanceApprover1Comment'].setValue(this.heroForm.value.comment);
                    if (this.heroForm.value.tempstatus == "Reject")
                        this.heroForm.controls['status'].setValue("Reject");
                    else if (this.heroForm.value.tempstatus == "Approved")
                        this.heroForm.controls['status'].setValue("Pending");

                    if (this.heroForm.value.status == 'Pending') {
                        this.heroForm.controls['wfCurrentApprover'].setValue("BU2Approver");
                        this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfbU2Approver);
                        if (this.heroForm.value.wfbU2Approver == null || this.heroForm.value.wfbU2Approver == "") {
                            if (this.heroForm.value.wfFinanceApprover1Approval == "Approved") {
                                this.heroForm.controls['wfCurrentApprover'].setValue("GlobalFinanceApprover1");
                                this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfGlobalFinanceApprover1);
                            }
                            this.heroForm.controls['wfbU2ApproverApproval'].setValue("Approved");
                            this.heroForm.controls['wfbU2ApproverComment'].setValue("No Business Unit Approver 2, default to approved");
                            this.heroForm.controls['status'].setValue("Pending");
                        }
                        if (this.heroForm.value.wfGlobalFinanceApprover1 == null || this.heroForm.value.wfGlobalFinanceApprover1 == "") {
                            if (this.heroForm.value.wfbU2ApproverApproval == "Approved") {
                                this.heroForm.controls['wfCurrentApprover'].setValue("FinalApprover5");
                                this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfFinalApprover5);
                            }
                            this.heroForm.controls['wfGlobalFinanceApprover1Approval'].setValue("Approved");
                            this.heroForm.controls['wfGlobalFinanceApprover1Comment'].setValue("No Global Finance Approver 1, default to approved");
                            this.heroForm.controls['status'].setValue("Pending");
                        }
                        if (this.heroForm.value.wfFinalApprover5 == null || this.heroForm.value.wfFinalApprover5 == "") {
                            if (this.heroForm.value.wfGlobalFinanceApprover1Approval == "Approved") {
                                this.heroForm.controls['wfCurrentApprover'].setValue("FinalApprover6");
                                this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfFinalApprover6);
                            }
                            this.heroForm.controls['wfFinalApprover5Approval'].setValue("Approved");
                            this.heroForm.controls['wfFinalApprover5Comment'].setValue("No Final Approver 5, default to approved");
                            this.heroForm.controls['status'].setValue("Pending");
                        }
                        if (this.heroForm.value.wfFinalApprover6 == null || this.heroForm.value.wfFinalApprover6 == "") {
                            if (this.heroForm.value.wfFinalApprover5Approval == "Approved") {
                                this.heroForm.controls['wfCurrentApprover'].setValue("Done");
                                this.heroForm.controls['wfFinalApprover6Approval'].setValue("Approved");
                                this.heroForm.controls['wfFinalApprover6Comment'].setValue("No Final Approver 6, default to approved");
                                this.heroForm.controls['status'].setValue("Approved");
                            }
                        }
                    }
                }
            }
            else if (newDate < appDate) {
                if (this.heroForm.value.wfbU2ApproverApproval == "Approved" && this.heroForm.value.wfFinanceApprover1Approval == "") {
                    this.heroForm.controls['wfFinanceApprover1Approval'].setValue(this.heroForm.value.tempstatus);
                    this.heroForm.controls['wfFinanceApprover1Comment'].setValue(this.heroForm.value.comment);
                    if (this.heroForm.value.tempstatus == "Reject")
                        this.heroForm.controls['status'].setValue("Reject");
                    else if (this.heroForm.value.tempstatus == "Approved")
                        this.heroForm.controls['status'].setValue("Pending");

                    if (this.heroForm.value.status == 'Pending') {
                        this.heroForm.controls['wfCurrentApprover'].setValue("GlobalFinanceApprover1");
                        this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfGlobalFinanceApprover1);

                        if (this.heroForm.value.wfGlobalFinanceApprover1 == null || this.heroForm.value.wfGlobalFinanceApprover1 == "") {
                            if (this.heroForm.value.wfFinanceApprover1Approval == "Approved") {
                                this.heroForm.controls['wfCurrentApprover'].setValue("FinalApprover1");
                                this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfFinalApprover1);
                            }
                            this.heroForm.controls['wfGlobalFinanceApprover1Approval'].setValue("Approved");
                            this.heroForm.controls['wfGlobalFinanceApprover1Comment'].setValue("No Global Finance Approver 1, default to approved");
                            this.heroForm.controls['status'].setValue("Pending");
                        }
                        if (this.heroForm.value.wfFinalApprover1 == null || this.heroForm.value.wfFinalApprover1 == "") {

                            if (this.heroForm.value.wfGlobalFinanceApprover1Approval == "Approved") {
                                this.heroForm.controls['wfCurrentApprover'].setValue("FinalApprover2");
                                this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfFinalApprover2);
                                this.heroForm.controls['wfFinalApprover1Approval'].setValue("Approved");
                                this.heroForm.controls['wfFinalApprover1Comment'].setValue("No Final Approver 1, default to approved");
                                this.heroForm.controls['status'].setValue("Pending");
                            }
                        }
                        if (this.heroForm.value.wfFinalApprover2 == null || this.heroForm.value.wfFinalApprover2 == "") {
                            if (this.heroForm.value.wfFinalApprover1Approval == "Approved") {
                                this.heroForm.controls['wfCurrentApprover'].setValue("FinalApprover3");
                                this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfFinalApprover3);
                                this.heroForm.controls['wfFinalApprover2Approval'].setValue("Approved");
                                this.heroForm.controls['wfFinalApprover2Comment'].setValue("No Final Approver 2, default to approved");
                                this.heroForm.controls['status'].setValue("Pending");
                            }
                        }
                        if (this.heroForm.value.wfFinalApprover3 == null || this.heroForm.value.wfFinalApprover3 == "") {
                            if (this.heroForm.value.wfFinalApprover2Approval == "Approved") {
                                this.heroForm.controls['wfCurrentApprover'].setValue("FinalApprover4");
                                this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfFinalApprover4);
                                this.heroForm.controls['wfFinalApprover3Approval'].setValue("Approved");
                                this.heroForm.controls['wfFinalApprover3Comment'].setValue("No Final Approver 3, default to approved");
                                this.heroForm.controls['status'].setValue("Pending");
                            }
                        }
                        if (this.heroForm.value.wfFinalApprover4 == null || this.heroForm.value.wfFinalApprover4 == "") {
                            if (this.heroForm.value.wfFinalApprover3Approval == "Approved") {
                                this.heroForm.controls['wfCurrentApprover'].setValue("FinalApprover5");
                                this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfFinalApprover5);
                                this.heroForm.controls['wfFinalApprover4Approval'].setValue("Approved");
                                this.heroForm.controls['wfFinalApprover4Comment'].setValue("No Final Approver 4, default to approved");
                                this.heroForm.controls['status'].setValue("Pending");
                            }
                        }


                        if (this.heroForm.value.wfFinalApprover5 == null || this.heroForm.value.wfFinalApprover5 == "") {
                            if (this.heroForm.value.wfFinalApprover4Approval == "Approved") {
                                this.heroForm.controls['wfCurrentApprover'].setValue("FinalApprover6");
                                this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfFinalApprover6);
                            }
                            this.heroForm.controls['wfFinalApprover5Approval'].setValue("Approved");
                            this.heroForm.controls['wfFinalApprover5Comment'].setValue("No Final Approver 5, default to approved");
                            this.heroForm.controls['status'].setValue("Pending");
                        }
                        if (this.heroForm.value.wfFinalApprover6 == null || this.heroForm.value.wfFinalApprover6 == "") {
                            if (this.heroForm.value.wfFinalApprover5Approval == "Approved") {
                                this.heroForm.controls['wfCurrentApprover'].setValue("Done");
                                this.heroForm.controls['wfFinalApprover6Approval'].setValue("Approved");
                                this.heroForm.controls['wfFinalApprover6Comment'].setValue("No Final Approver 6, default to approved");
                                this.heroForm.controls['status'].setValue("Approved");
                            }
                        }
                    }
                }
            }

        }
        else if (this.heroForm.value.wfCurrentApprover == "BU2Approver") {
            if (newDate > appDate) {
                if (this.heroForm.value.wfFinanceApprover1Approval == "Approved" && (this.heroForm.value.wfbU2ApproverApproval == "" || this.heroForm.value.wfbU2ApproverApproval == "null")) {

                    this.heroForm.controls['wfbU2ApproverApproval'].setValue(this.heroForm.value.tempstatus);
                    this.heroForm.controls['wfbU2ApproverComment'].setValue(this.heroForm.value.comment);
                    if (this.heroForm.value.tempstatus == "Reject")
                        this.heroForm.controls['status'].setValue("Reject");
                    else if (this.heroForm.value.tempstatus == "Approved")
                        this.heroForm.controls['status'].setValue("Pending");

                    if (this.heroForm.value.status == 'Pending') {
                        this.heroForm.controls['wfCurrentApprover'].setValue("GlobalFinanceApprover1");
                        this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfGlobalFinanceApprover1);

                        if (this.heroForm.value.wfGlobalFinanceApprover1 == null || this.heroForm.value.wfGlobalFinanceApprover1 == "") {
                            if (this.heroForm.value.wfbU2ApproverApproval == "Approved") {
                                this.heroForm.controls['wfCurrentApprover'].setValue("FinalApprover5");
                                this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfFinalApprover5);
                                this.heroForm.controls['wfGlobalFinanceApprover1Approval'].setValue("Approved");
                                this.heroForm.controls['wfGlobalFinanceApprover1Comment'].setValue("No Global Finance Approver 1, default to approved");
                                this.heroForm.controls['status'].setValue("Pending");
                            }
                        }
                        if (this.heroForm.value.wfFinalApprover5 == null || this.heroForm.value.wfFinalApprover5 == "") {
                            if (this.heroForm.value.wfGlobalFinanceApprover1Approval == "Approved") {
                                this.heroForm.controls['wfCurrentApprover'].setValue("FinalApprover6");
                                this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfFinalApprover6);
                                this.heroForm.controls['wfFinalApprover5Approval'].setValue("Approved");
                                this.heroForm.controls['wfFinalApprover5Comment'].setValue("No Final Approver 5, default to approved");
                                this.heroForm.controls['status'].setValue("Pending");
                            }
                        }
                        if (this.heroForm.value.wfFinalApprover6 == null || this.heroForm.value.wfFinalApprover6 == "") {
                            if (this.heroForm.value.wfFinalApprover5Approval == "Approved") {
                                this.heroForm.controls['wfCurrentApprover'].setValue("Done");
                                this.heroForm.controls['wfFinalApprover6Approval'].setValue("Approved");
                                this.heroForm.controls['wfFinalApprover6Comment'].setValue("No Final Approver 6, default to approved");
                                this.heroForm.controls['status'].setValue("Approved");
                            }
                        }

                    }
                }
            }
            else if (newDate < appDate) {
                if (this.heroForm.value.wfbU1ApproverApproval == "Approved" && (this.heroForm.value.wfbU2ApproverApproval == "" || this.heroForm.value.wfbU2ApproverApproval == "null")) {

                    this.heroForm.controls['wfbU2ApproverApproval'].setValue(this.heroForm.value.tempstatus);
                    this.heroForm.controls['wfbU2ApproverComment'].setValue(this.heroForm.value.comment);
                    if (this.heroForm.value.tempstatus == "Reject")
                        this.heroForm.controls['status'].setValue("Reject");
                    else if (this.heroForm.value.tempstatus == "Approved")
                        this.heroForm.controls['status'].setValue("Pending");

                    if (this.heroForm.value.status == 'Pending') {
                        this.heroForm.controls['wfCurrentApprover'].setValue("FinanceApprover1");
                        this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfFinanceApprover1);

                        if (this.heroForm.value.wfFinanceApprover1 == null || this.heroForm.value.wfFinanceApprover1 == "") {
                            if (this.heroForm.value.wfbU2ApproverApproval == "Approved") {
                                this.heroForm.controls['wfCurrentApprover'].setValue("GlobalFinanceApprover1");
                                this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfGlobalFinanceApprover1);
                                this.heroForm.controls['wfFinanceApprover1Approval'].setValue("Approved");
                                this.heroForm.controls['wfFinanceApprover1Comment'].setValue("No Finance Approver 1, default to approved");
                                this.heroForm.controls['status'].setValue("Pending");
                            }
                        }
                        if (this.heroForm.value.wfGlobalFinanceApprover1 == null || this.heroForm.value.wfGlobalFinanceApprover1 == "") {
                            if (this.heroForm.value.wfFinanceApprover1Approval == "Approved") {
                                this.heroForm.controls['wfCurrentApprover'].setValue("FinalApprover1");
                                this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfFinalApprover1);
                                this.heroForm.controls['wfGlobalFinanceApprover1Approval'].setValue("Approved");
                                this.heroForm.controls['wfGlobalFinanceApprover1Comment'].setValue("No Global Finance Approver 1, default to approved");
                                this.heroForm.controls['status'].setValue("Pending");
                            }
                        }
                        if (this.heroForm.value.wfFinalApprover1 == null || this.heroForm.value.wfFinalApprover1 == "") {
                            if (this.heroForm.value.wfGlobalFinanceApprover1Approval == "Approved") {
                                this.heroForm.controls['wfCurrentApprover'].setValue("FinalApprover2");
                                this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfFinalApprover2);
                                this.heroForm.controls['wfFinalApprover1Approval'].setValue("Approved");
                                this.heroForm.controls['wfFinalApprover1Comment'].setValue("No Final Approver 1, default to approved");
                                this.heroForm.controls['status'].setValue("Pending");
                            }
                        }
                        if (this.heroForm.value.wfFinalApprover2 == null || this.heroForm.value.wfFinalApprover2 == "") {
                            if (this.heroForm.value.wfFinalApprover1Approval == "Approved") {
                                this.heroForm.controls['wfCurrentApprover'].setValue("FinalApprover3");
                                this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfFinalApprover3);
                                this.heroForm.controls['wfFinalApprover2Approval'].setValue("Approved");
                                this.heroForm.controls['wfFinalApprover2Comment'].setValue("No Final Approver 2, default to approved");
                                this.heroForm.controls['status'].setValue("Pending");
                            }
                        }
                        if (this.heroForm.value.wfFinalApprover3 == null || this.heroForm.value.wfFinalApprover3 == "") {
                            if (this.heroForm.value.wfFinalApprover2Approval == "Approved") {
                                this.heroForm.controls['wfCurrentApprover'].setValue("FinalApprover4");
                                this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfFinalApprover4);
                                this.heroForm.controls['wfFinalApprover3Approval'].setValue("Approved");
                                this.heroForm.controls['wfFinalApprover3Comment'].setValue("No Final Approver 3, default to approved");
                                this.heroForm.controls['status'].setValue("Pending");
                            }
                        }
                        if (this.heroForm.value.wfFinalApprover4 == null || this.heroForm.value.wfFinalApprover4 == "") {
                            if (this.heroForm.value.wfFinalApprover3Approval == "Approved") {
                                this.heroForm.controls['wfCurrentApprover'].setValue("FinalApprover5");
                                this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfFinalApprover5);
                                this.heroForm.controls['wfFinalApprover4Approval'].setValue("Approved");
                                this.heroForm.controls['wfFinalApprover4Comment'].setValue("No Final Approver 4, default to approved");
                                this.heroForm.controls['status'].setValue("Pending");
                            }
                        }
                        if (this.heroForm.value.wfFinalApprover5 == null || this.heroForm.value.wfFinalApprover5 == "") {
                            if (this.heroForm.value.wfFinalApprover4Approval == "Approved") {
                                this.heroForm.controls['wfCurrentApprover'].setValue("FinalApprover6");
                                this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfFinalApprover6);
                                this.heroForm.controls['wfFinalApprover5Approval'].setValue("Approved");
                                this.heroForm.controls['wfFinalApprover5Comment'].setValue("No Final Approver 5, default to approved");
                                this.heroForm.controls['status'].setValue("Pending");
                            }
                        }
                        if (this.heroForm.value.wfFinalApprover6 == null || this.heroForm.value.wfFinalApprover6 == "") {
                            if (this.heroForm.value.wfFinalApprover5Approval == "Approved") {
                                this.heroForm.controls['wfCurrentApprover'].setValue("Done");
                                this.heroForm.controls['wfFinalApprover6Approval'].setValue("Approved");
                                this.heroForm.controls['wfFinalApprover6Comment'].setValue("No Final Approver 6, default to approved");
                                this.heroForm.controls['status'].setValue("Approved");
                            }
                        }
                    }
                }
            }

        }

        else if (this.heroForm.value.wfCurrentApprover == "GlobalFinanceApprover1") {
            if (newDate > appDate) {
                if (this.heroForm.value.wfbU2ApproverApproval == "Approved" && this.heroForm.value.wfGlobalFinanceApprover1Approval == "") {
                    this.heroForm.controls['wfGlobalFinanceApprover1Approval'].setValue(this.heroForm.value.tempstatus);
                    this.heroForm.controls['wfGlobalFinanceApprover1Comment'].setValue(this.heroForm.value.comment);
                    if (this.heroForm.value.tempstatus == "Reject")
                        this.heroForm.controls['status'].setValue("Reject");
                    else if (this.heroForm.value.tempstatus == "Approved")
                        this.heroForm.controls['status'].setValue("Pending");


                    if (this.heroForm.value.status == 'Pending') {
                        this.heroForm.controls['wfCurrentApprover'].setValue("FinalApprover5");
                        this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfFinalApprover5);

                        if (this.heroForm.value.wfFinalApprover5 == null || this.heroForm.value.wfFinalApprover5 == "") {
                            if (this.heroForm.value.wfGlobalFinanceApprover1Approval == "Approved") {
                                this.heroForm.controls['wfCurrentApprover'].setValue("FinalApprover6");
                                this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfFinalApprover6);
                                this.heroForm.controls['wfFinalApprover5Approval'].setValue("Approved");
                                this.heroForm.controls['wfFinalApprover5Comment'].setValue("No Final Approver 5, default to approved");
                                this.heroForm.controls['status'].setValue("Pending");
                            }
                        }
                        if (this.heroForm.value.wfFinalApprover6 == null || this.heroForm.value.wfFinalApprover6 == "") {
                            if (this.heroForm.value.wfFinalApprover5Approval == "Approved") {
                                this.heroForm.controls['wfCurrentApprover'].setValue("Done");
                                this.heroForm.controls['wfFinalApprover6Approval'].setValue("Approved");
                                this.heroForm.controls['wfFinalApprover6Comment'].setValue("No Final Approver 6, default to approved");
                                this.heroForm.controls['status'].setValue("Approved");
                            }
                        }
                    }
                }
            }
            else if (newDate < appDate) {
                if (this.heroForm.value.wfFinanceApprover1Approval == "Approved" && this.heroForm.value.wfGlobalFinanceApprover1Approval == "") {
                    if (_actamt > 50000) {
                        this.heroForm.controls['wfGlobalFinanceApprover1Approval'].setValue(this.heroForm.value.tempstatus);
                        this.heroForm.controls['wfGlobalFinanceApprover1Comment'].setValue(this.heroForm.value.comment);
                        if (this.heroForm.value.tempstatus == "Reject")
                            this.heroForm.controls['status'].setValue("Reject");
                        else if (this.heroForm.value.tempstatus == "Approved")
                            this.heroForm.controls['status'].setValue("Pending");

                    }
                    else {
                        this.heroForm.controls['wfGlobalFinanceApprover1Approval'].setValue("Approved");
                        this.heroForm.controls['wfGlobalFinanceApprover1Comment'].setValue("Act amount not in range.");
                    }

                    if (this.heroForm.value.status == 'Pending') {
                        this.heroForm.controls['wfCurrentApprover'].setValue("FinalApprover1");
                        this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfFinalApprover1);

                        if (this.heroForm.value.wfFinalApprover1 == null || this.heroForm.value.wfFinalApprover1 == "") {
                            if (this.heroForm.value.wfGlobalFinanceApprover1Approval == "Approved") {
                                this.heroForm.controls['wfCurrentApprover'].setValue("FinalApprover2");
                                this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfFinalApprover2);
                                this.heroForm.controls['wfFinalApprover1Approval'].setValue("Approved");
                                this.heroForm.controls['wfFinalApprover1Comment'].setValue("No Final Approver 1, default to approved");
                                this.heroForm.controls['status'].setValue("Pending");
                            }
                        }
                        if (this.heroForm.value.wfFinalApprover2 == null || this.heroForm.value.wfFinalApprover2 == "") {
                            if (this.heroForm.value.wfFinalApprover1Approval == "Approved") {
                                this.heroForm.controls['wfCurrentApprover'].setValue("FinalApprover3");
                                this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfFinalApprover3);
                                this.heroForm.controls['wfFinalApprover2Approval'].setValue("Approved");
                                this.heroForm.controls['wfFinalApprover2Comment'].setValue("No Final Approver 2, default to approved");
                                this.heroForm.controls['status'].setValue("Pending");
                            }
                        }
                        if (this.heroForm.value.wfFinalApprover3 == null || this.heroForm.value.wfFinalApprover3 == "") {
                            if (this.heroForm.value.wfFinalApprover2Approval == "Approved") {
                                this.heroForm.controls['wfCurrentApprover'].setValue("FinalApprover4");
                                this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfFinalApprover4);
                                this.heroForm.controls['wfFinalApprover3Approval'].setValue("Approved");
                                this.heroForm.controls['wfFinalApprover3Comment'].setValue("No Final Approver 3, default to approved");
                                this.heroForm.controls['status'].setValue("Pending");
                            }
                        }
                        if (this.heroForm.value.wfFinalApprover4 == null || this.heroForm.value.wfFinalApprover4 == "") {
                            if (this.heroForm.value.wfFinalApprover3Approval == "Approved") {
                                this.heroForm.controls['wfCurrentApprover'].setValue("FinalApprover5");
                                this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfFinalApprover5);
                                this.heroForm.controls['wfFinalApprover4Approval'].setValue("Approved");
                                this.heroForm.controls['wfFinalApprover4Comment'].setValue("No Final Approver 4, default to approved");
                                this.heroForm.controls['status'].setValue("Pending");
                            }
                        }
                        if (this.heroForm.value.wfFinalApprover5 == null || this.heroForm.value.wfFinalApprover5 == "") {
                            if (this.heroForm.value.wfFinalApprover4Approval == "Approved") {
                                this.heroForm.controls['wfCurrentApprover'].setValue("FinalApprover6");
                                this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfFinalApprover6);
                                this.heroForm.controls['wfFinalApprover5Approval'].setValue("Approved");
                                this.heroForm.controls['wfFinalApprover5Comment'].setValue("No Final Approver 5, default to approved");
                                this.heroForm.controls['status'].setValue("Pending");
                            }
                        }
                        if (this.heroForm.value.wfFinalApprover6 == null || this.heroForm.value.wfFinalApprover6 == "") {
                            if (this.heroForm.value.wfFinalApprover5Approval == "Approved") {
                                this.heroForm.controls['wfCurrentApprover'].setValue("Done");
                                this.heroForm.controls['wfFinalApprover6Approval'].setValue("Approved");
                                this.heroForm.controls['wfFinalApprover6Comment'].setValue("No Final Approver 6, default to approved");
                                this.heroForm.controls['status'].setValue("Approved");
                            }
                        }
                    }
                }
            }
        }
        else if (this.heroForm.value.wfCurrentApprover == "FinalApprover1") {
            if (newDate < appDate) {
                if (this.heroForm.value.wfGlobalFinanceApprover1Approval == "Approved" && this.heroForm.value.wfFinalApprover1Approval == "") {
                    if (_actamt > 100000) {
                        this.heroForm.controls['wfFinalApprover1Approval'].setValue(this.heroForm.value.tempstatus);
                        this.heroForm.controls['wfFinalApprover1Comment'].setValue(this.heroForm.value.comment);
                        if (this.heroForm.value.tempstatus == "Reject")
                            this.heroForm.controls['status'].setValue("Reject");
                        else if (this.heroForm.value.tempstatus == "Approved")
                            this.heroForm.controls['status'].setValue("Pending");
                    }
                    else {
                        this.heroForm.controls['wfFinalApprover1Approval'].setValue("Approved");
                        this.heroForm.controls['wfFinalApprover1Comment'].setValue("Act amount not in range.");
                    }
                    if (this.heroForm.value.status == 'Pending') {
                        this.heroForm.controls['wfCurrentApprover'].setValue("FinalApprover2");
                        this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfFinalApprover2);
                        if (this.heroForm.value.wfFinalApprover2 == null || this.heroForm.value.wfFinalApprover2 == "") {
                            if (this.heroForm.value.wfFinalApprover1Approval == "Approved") {
                                this.heroForm.controls['wfCurrentApprover'].setValue("FinalApprover3");
                                this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfFinalApprover3);
                                this.heroForm.controls['wfFinalApprover2Approval'].setValue("Approved");
                                this.heroForm.controls['wfFinalApprover2Comment'].setValue("No Final Approver 2, default to approved");
                                this.heroForm.controls['status'].setValue("Pending");
                            }
                        }
                        if (this.heroForm.value.wfFinalApprover3 == null || this.heroForm.value.wfFinalApprover3 == "") {
                            if (this.heroForm.value.wfFinalApprover2Approval == "Approved") {
                                this.heroForm.controls['wfCurrentApprover'].setValue("FinalApprover4");
                                this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfFinalApprover4);
                                this.heroForm.controls['wfFinalApprover3Approval'].setValue("Approved");
                                this.heroForm.controls['wfFinalApprover3Comment'].setValue("No Final Approver 3, default to approved");
                                this.heroForm.controls['status'].setValue("Pending");
                            }
                        }
                        if (this.heroForm.value.wfFinalApprover4 == null || this.heroForm.value.wfFinalApprover4 == "") {
                            if (this.heroForm.value.wfFinalApprover3Approval == "Approved") {
                                this.heroForm.controls['wfCurrentApprover'].setValue("FinalApprover5");
                                this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfFinalApprover5);
                                this.heroForm.controls['wfFinalApprover4Approval'].setValue("Approved");
                                this.heroForm.controls['wfFinalApprover4Comment'].setValue("No Final Approver 4, default to approved");
                                this.heroForm.controls['status'].setValue("Pending");
                            }
                        }
                        if (this.heroForm.value.wfFinalApprover5 == null || this.heroForm.value.wfFinalApprover5 == "") {
                            if (this.heroForm.value.wfFinalApprover4Approval == "Approved") {
                                this.heroForm.controls['wfCurrentApprover'].setValue("FinalApprover6");
                                this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfFinalApprover6);
                                this.heroForm.controls['wfFinalApprover5Approval'].setValue("Approved");
                                this.heroForm.controls['wfFinalApprover5Comment'].setValue("No Final Approver 5, default to approved");
                                this.heroForm.controls['status'].setValue("Pending");
                            }
                        }
                        if (this.heroForm.value.wfFinalApprover6 == null || this.heroForm.value.wfFinalApprover6 == "") {
                            if (this.heroForm.value.wfFinalApprover5Approval == "Approved") {
                                this.heroForm.controls['wfCurrentApprover'].setValue("Done");
                                this.heroForm.controls['wfFinalApprover6Approval'].setValue("Approved");
                                this.heroForm.controls['wfFinalApprover6Comment'].setValue("No Final Approver 6, default to approved");
                                this.heroForm.controls['status'].setValue("Approved");
                            }
                        }
                    }
                }
            }
        }
        else if (this.heroForm.value.wfCurrentApprover == "FinalApprover2") {
            if (newDate < appDate) {
                if (this.heroForm.value.wfFinanceApprover1Approval == "Approved" && this.heroForm.value.wfFinalApprover2Approval == "") {
                    if (_actamt > 250000) {
                        this.heroForm.controls['wfFinalApprover2Approval'].setValue(this.heroForm.value.tempstatus);
                        this.heroForm.controls['wfFinalApprover2Comment'].setValue(this.heroForm.value.comment);
                        if (this.heroForm.value.tempstatus == "Reject")
                            this.heroForm.controls['status'].setValue("Reject");
                        else if (this.heroForm.value.tempstatus == "Approved")
                            this.heroForm.controls['status'].setValue("Pending");
                    }
                    else {
                        this.heroForm.controls['wfFinalApprover2Approval'].setValue("Approved");
                        this.heroForm.controls['wfFinalApprover2Comment'].setValue("Act amount not in range.");
                    }
                    if (this.heroForm.value.status == 'Pending') {
                        this.heroForm.controls['wfCurrentApprover'].setValue("FinalApprover3");
                        this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfFinalApprover3);

                        if (this.heroForm.value.wfFinalApprover3 == null || this.heroForm.value.wfFinalApprover3 == "") {
                            if (this.heroForm.value.wfFinalApprover2Approval == "Approved") {
                                this.heroForm.controls['wfCurrentApprover'].setValue("FinalApprover4");
                                this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfFinalApprover4);
                                this.heroForm.controls['wfFinalApprover3Approval'].setValue("Approved");
                                this.heroForm.controls['wfFinalApprover3Comment'].setValue("No Final Approver 3, default to approved");
                                this.heroForm.controls['status'].setValue("Pending");
                            }
                        }
                        if (this.heroForm.value.wfFinalApprover4 == null || this.heroForm.value.wfFinalApprover4 == "") {
                            if (this.heroForm.value.wfFinalApprover3Approval == "Approved") {
                                this.heroForm.controls['wfCurrentApprover'].setValue("FinalApprover5");
                                this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfFinalApprover5);
                                this.heroForm.controls['wfFinalApprover4Approval'].setValue("Approved");
                                this.heroForm.controls['wfFinalApprover4Comment'].setValue("No Final Approver 4, default to approved");
                                this.heroForm.controls['status'].setValue("Pending");
                            }
                        }
                        if (this.heroForm.value.wfFinalApprover5 == null || this.heroForm.value.wfFinalApprover5 == "") {
                            if (this.heroForm.value.wfFinalApprover4Approval == "Approved") {
                                this.heroForm.controls['wfCurrentApprover'].setValue("FinalApprover6");
                                this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfFinalApprover6);
                                this.heroForm.controls['wfFinalApprover5Approval'].setValue("Approved");
                                this.heroForm.controls['wfFinalApprover5Comment'].setValue("No Final Approver 5, default to approved");
                                this.heroForm.controls['status'].setValue("Pending");
                            }
                        }
                        if (this.heroForm.value.wfFinalApprover6 == null || this.heroForm.value.wfFinalApprover6 == "") {
                            if (this.heroForm.value.wfFinalApprover5Approval == "Approved") {
                                this.heroForm.controls['wfCurrentApprover'].setValue("Done");
                                this.heroForm.controls['wfFinalApprover6Approval'].setValue("Approved");
                                this.heroForm.controls['wfFinalApprover6Comment'].setValue("No Final Approver 6, default to approved");
                                this.heroForm.controls['status'].setValue("Approved");
                            }
                        }
                    }
                }
            }
        }
        else if (this.heroForm.value.wfCurrentApprover == "FinalApprover3") {
            if (newDate < appDate) {
                if (this.heroForm.value.wfFinanceApprover2Approval == "Approved" && this.heroForm.value.wfFinalApprover3Approval == "") {

                    if (_actamt > 500000) {
                        this.heroForm.controls['wfFinalApprover3Approval'].setValue(this.heroForm.value.tempstatus);
                        this.heroForm.controls['wfFinalApprover3Comment'].setValue(this.heroForm.value.comment);
                        if (this.heroForm.value.tempstatus == "Reject")
                            this.heroForm.controls['status'].setValue("Reject");
                        else if (this.heroForm.value.tempstatus == "Approved")
                            this.heroForm.controls['status'].setValue("Pending");
                    }
                    else {
                        this.heroForm.controls['wfFinalApprover3Approval'].setValue("Approved");
                        this.heroForm.controls['wfFinalApprover3Comment'].setValue("Act amount not in range.");

                    }

                    if (this.heroForm.value.status == 'Pending') {
                        this.heroForm.controls['wfCurrentApprover'].setValue("FinalApprover4");
                        this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfFinalApprover4);

                        if (this.heroForm.value.wfFinalApprover4 == null || this.heroForm.value.wfFinalApprover4 == "") {
                            if (this.heroForm.value.wfFinalApprover3Approval == "Approved") {
                                this.heroForm.controls['wfCurrentApprover'].setValue("FinalApprover5");
                                this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfFinalApprover5);
                                this.heroForm.controls['wfFinalApprover4Approval'].setValue("Approved");
                                this.heroForm.controls['wfFinalApprover4Comment'].setValue("No Final Approver 4, default to approved");
                                this.heroForm.controls['status'].setValue("Pending");
                            }
                        }
                        if (this.heroForm.value.wfFinalApprover5 == null || this.heroForm.value.wfFinalApprover5 == "") {
                            if (this.heroForm.value.wfFinalApprover4Approval == "Approved") {
                                this.heroForm.controls['wfCurrentApprover'].setValue("FinalApprover6");
                                this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfFinalApprover6);
                                this.heroForm.controls['wfFinalApprover5Approval'].setValue("Approved");
                                this.heroForm.controls['wfFinalApprover5Comment'].setValue("No Final Approver 5, default to approved");
                                this.heroForm.controls['status'].setValue("Pending");
                            }
                        }
                        if (this.heroForm.value.wfFinalApprover6 == null || this.heroForm.value.wfFinalApprover6 == "") {
                            if (this.heroForm.value.wfFinalApprover5Approval == "Approved") {
                                this.heroForm.controls['wfCurrentApprover'].setValue("Done");
                                this.heroForm.controls['wfFinalApprover6Approval'].setValue("Approved");
                                this.heroForm.controls['wfFinalApprover6Comment'].setValue("No Final Approver 6, default to approved");
                                this.heroForm.controls['status'].setValue("Approved");
                            }
                        }
                    }
                }
            }
        }
        else if (this.heroForm.value.wfCurrentApprover == "FinalApprover4") {
            if (newDate < appDate) {
                if (this.heroForm.value.wfFinanceApprover3Approval == "Approved" && this.heroForm.value.wfFinalApprover4Approval == "") {
                    if (_actamt > 500000) {
                        this.heroForm.controls['wfFinalApprover4Approval'].setValue(this.heroForm.value.tempstatus);
                        this.heroForm.controls['wfFinalApprover4Comment'].setValue(this.heroForm.value.comment);
                        if (this.heroForm.value.tempstatus == "Reject")
                            this.heroForm.controls['status'].setValue("Reject");
                        else if (this.heroForm.value.tempstatus == "Approved")
                            this.heroForm.controls['status'].setValue("Pending");
                    }
                    else {
                        this.heroForm.controls['wfFinalApprover4Approval'].setValue("Approved");
                        this.heroForm.controls['wfFinalApprover4Comment'].setValue("Act amount not in range.");
                    }

                    if (this.heroForm.value.status == 'Pending') {
                        this.heroForm.controls['wfCurrentApprover'].setValue("FinalApprover5");
                        this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfFinalApprover5);

                        if (this.heroForm.value.wfFinalApprover5 == null || this.heroForm.value.wfFinalApprover5 == "") {
                            if (this.heroForm.value.wfFinalApprover4Approval == "Approved") {
                                this.heroForm.controls['wfCurrentApprover'].setValue("FinalApprover6");
                                this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfFinalApprover6);
                                this.heroForm.controls['wfFinalApprover5Approval'].setValue("Approved");
                                this.heroForm.controls['wfFinalApprover5Comment'].setValue("No Final Approver 5, default to approved");
                                this.heroForm.controls['status'].setValue("Pending");
                            }
                        }
                        if (this.heroForm.value.wfFinalApprover6 == null || this.heroForm.value.wfFinalApprover6 == "") {
                            if (this.heroForm.value.wfFinalApprover5Approval == "Approved") {
                                this.heroForm.controls['wfCurrentApprover'].setValue("Done");
                                this.heroForm.controls['wfFinalApprover6Approval'].setValue("Approved");
                                this.heroForm.controls['wfFinalApprover6Comment'].setValue("No Final Approver 6, default to approved");
                                this.heroForm.controls['status'].setValue("Approved");
                            }
                        }
                    }
                }
            }
        }
        else if (this.heroForm.value.wfCurrentApprover == "FinalApprover5") {
            if (newDate > appDate) {
                if (this.heroForm.value.wfGlobalFinanceApprover1Approval == "Approved" && this.heroForm.value.wfFinalApprover5Approval == "") {
                    if (_actamt > 50000) {
                        this.heroForm.controls['wfFinalApprover5Approval'].setValue(this.heroForm.value.tempstatus);
                        this.heroForm.controls['wfFinalApprover5Comment'].setValue(this.heroForm.value.comment);
                        if (this.heroForm.value.tempstatus == "Reject")
                            this.heroForm.controls['status'].setValue("Reject");
                        else if (this.heroForm.value.tempstatus == "Approved") {
                            // this.heroForm.controls['status'].setValue("Pending");
                            this.heroForm.controls['wfFinalApprover6Approval'].setValue("Approved");
                            this.heroForm.controls['status'].setValue("Approved");
                            this.heroForm.controls['wfCurrentApprover'].setValue("Done");
                        }
                    }
                    else {
                        this.heroForm.controls['wfFinalApprover5Approval'].setValue("Approved");
                        this.heroForm.controls['wfFinalApprover5Comment'].setValue("Act amount not in range.");
                    }

                    if (this.heroForm.value.status == 'Pending') {
                        this.heroForm.controls['wfCurrentApprover'].setValue("FinalApprover6");
                        this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfFinalApprover6);

                        if (this.heroForm.value.wfFinalApprover6 == null || this.heroForm.value.wfFinalApprover6 == "") {
                            if (this.heroForm.value.wfFinalApprover5Approval == "Approved") {
                                this.heroForm.controls['wfCurrentApprover'].setValue("Done");
                                this.heroForm.controls['wfFinalApprover6Approval'].setValue("Approved");
                                this.heroForm.controls['wfFinalApprover6Comment'].setValue("No Final Approver 6, default to approved");
                                this.heroForm.controls['status'].setValue("Approved");
                            }
                        }
                    }
                }
            }
            else if (newDate < appDate) {
                if (this.heroForm.value.wfFinalApprover4Approval == "Approved" && this.heroForm.value.wfFinalApprover5Approval == "") {
                    if (_actamt > 2000000) {
                        this.heroForm.controls['wfFinalApprover5Approval'].setValue(this.heroForm.value.tempstatus);
                        this.heroForm.controls['wfFinalApprover5Comment'].setValue(this.heroForm.value.comment);
                        if (this.heroForm.value.tempstatus == "Reject")
                            this.heroForm.controls['status'].setValue("Reject");
                        else if (this.heroForm.value.tempstatus == "Approved") {
                            // this.heroForm.controls['status'].setValue("Pending");
                            this.heroForm.controls['wfFinalApprover6Approval'].setValue("Approved");
                            this.heroForm.controls['status'].setValue("Approved");
                            this.heroForm.controls['wfCurrentApprover'].setValue("Done");

                        }
                    }
                    else {
                        this.heroForm.controls['wfFinalApprover5Approval'].setValue("Approved");
                        this.heroForm.controls['wfFinalApprover5Comment'].setValue("Act amount not in range.");
                    }


                    if (this.heroForm.value.status == 'Pending') {
                        this.heroForm.controls['wfCurrentApprover'].setValue("FinalApprover6");
                        this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfFinalApprover6);

                        if (this.heroForm.value.wfFinalApprover6 == null || this.heroForm.value.wfFinalApprover6 == "") {
                            if (this.heroForm.value.wfFinalApprover5Approval == "Approved") {
                                this.heroForm.controls['wfCurrentApprover'].setValue("Done");
                                this.heroForm.controls['wfFinalApprover6Approval'].setValue("Approved");
                                this.heroForm.controls['wfFinalApprover6Comment'].setValue("No Final Approver 6, default to approved");
                                this.heroForm.controls['status'].setValue("Approved");
                            }
                        }
                    }
                }

            }
        }
        else if (this.heroForm.value.wfCurrentApprover == "FinalApprover6") {
            if (newDate > appDate) {
                if (this.heroForm.value.wfFinalApprover5Approval == "Approved" && this.heroForm.value.wfFinalApprover6Approval == "") {
                    if (_actamt > 50000) {
                        this.heroForm.controls['wfFinalApprover6Approval'].setValue(this.heroForm.value.tempstatus);
                        this.heroForm.controls['wfFinalApprover6Comment'].setValue(this.heroForm.value.comment);
                        if (this.heroForm.value.tempstatus == "Reject")
                            this.heroForm.controls['status'].setValue("Reject");
                        else if (this.heroForm.value.tempstatus == "Approved")
                            this.heroForm.controls['status'].setValue("Approved");
                    }
                    else {
                        this.heroForm.controls['wfFinalApprover6Approval'].setValue("Approved");
                        this.heroForm.controls['wfFinalApprover6Comment'].setValue("Act amount not in range.");
                        this.heroForm.controls['status'].setValue("Approved");
                    }
                }

            }
            else if (newDate < appDate) {
                if (this.heroForm.value.wfFinalApprover5Approval == "Approved" && this.heroForm.value.wfFinalApprover6Approval == "") {
                    if (_actamt > 3000000) {
                        this.heroForm.controls['wfFinalApprover6Approval'].setValue(this.heroForm.value.tempstatus);
                        this.heroForm.controls['wfFinalApprover6Comment'].setValue(this.heroForm.value.comment);
                        if (this.heroForm.value.tempstatus == "Reject")
                            this.heroForm.controls['status'].setValue("Reject");
                        else if (this.heroForm.value.tempstatus == "Approved")
                            this.heroForm.controls['status'].setValue("Approved");
                    }
                    else {
                        this.heroForm.controls['wfFinalApprover6Approval'].setValue("Approved");
                        this.heroForm.controls['wfFinalApprover6Comment'].setValue("Act amount not in range.");
                        this.heroForm.controls['status'].setValue("Approved");
                    }
                }
            }
        }

        if (newDate > appDate) {
            if (_actamt <= 50000) {
                if (this.heroForm.value.wfCurrentApprover == "FinalApprover5" || this.heroForm.value.wfCurrentApprover == "FinalApprover6") {
                    this.heroForm.controls['wfFinalApprover5Approval'].setValue("Approved");
                    this.heroForm.controls['wfFinalApprover5Comment'].setValue("Act amount not in range.");
                    this.heroForm.controls['wfFinalApprover6Approval'].setValue("Approved");
                    this.heroForm.controls['wfFinalApprover6Comment'].setValue("Act amount not in range.");
                    this.heroForm.controls['wfCurrentApprover'].setValue("Done");
                    this.heroForm.controls['wfCurrentApproverName'].setValue("");
                    this.heroForm.controls['status'].setValue("Approved");

                }
            }
        }
        else if (newDate < appDate) {
            if (_actamt <= 50000) {
                if (this.heroForm.value.wfCurrentApprover == "GlobalFinanceApprover1") {
                    this.heroForm.controls['wfGlobalFinanceApprover1Approval'].setValue("Approved");
                    this.heroForm.controls['wfGlobalFinanceApprover1Comment'].setValue("Act amount not in range.");
                }
            }
            if (_actamt <= 100000) {
                if (this.heroForm.value.wfCurrentApprover == "FinalApprover1") {
                    this.heroForm.controls['wfFinalApprover1Approval'].setValue("Approved");
                    this.heroForm.controls['wfFinalApprover1Comment'].setValue("Act amount not in range.");
                }
            }
            if (_actamt <= 250000) {
                if (this.heroForm.value.wfCurrentApprover == "FinalApprover2") {
                    this.heroForm.controls['wfFinalApprover2Approval'].setValue("Approved");
                    this.heroForm.controls['wfFinalApprover2Comment'].setValue("Act amount not in range.");
                }
            }
            if (_actamt <= 500000) {
                if (this.heroForm.value.wfCurrentApprover == "FinalApprover3") {
                    this.heroForm.controls['wfFinalApprover3Approval'].setValue("Approved");
                    this.heroForm.controls['wfFinalApprover3Comment'].setValue("Act amount not in range.");
                }
                else if (this.heroForm.value.wfCurrentApprover == "FinalApprover4" ) {
                    this.heroForm.controls['wfFinalApprover4Approval'].setValue("Approved");
                    this.heroForm.controls['wfFinalApprover4Comment'].setValue("Act amount not in range.");
                }
            }
            if (_actamt <= 2000000) {
                if (this.heroForm.value.wfCurrentApprover == "FinalApprover5") {
                    this.heroForm.controls['wfFinalApprover5Approval'].setValue("Approved");
                    this.heroForm.controls['wfFinalApprover5Comment'].setValue("Act amount not in range.");
                }
            }
            if (_actamt <= 3000000) {
                if (this.heroForm.value.wfCurrentApprover == "FinalApprover6") {
                    this.heroForm.controls['wfFinalApprover6Approval'].setValue("Approved");
                    this.heroForm.controls['wfFinalApprover6Comment'].setValue("Act amount not in range.");
                    this.heroForm.controls['wfCurrentApprover'].setValue("Done");
                    this.heroForm.controls['wfCurrentApproverName'].setValue("");
                    this.heroForm.controls['status'].setValue("Approved");
                }
            }
        }
        console.log(this.heroForm.value);
        this.itemService.setApprovalStatus(this.heroForm.value.id, this.heroForm.value);

    }


    onBack(): void {
        this.router.navigate(['/view'])
    }
}
