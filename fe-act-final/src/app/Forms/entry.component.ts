import { OnInit, Component, ViewChild, AfterViewInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators, FormControl, FormArray } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { AuthService } from "../services/auth.service";
import { itemService } from '../items/item.service';
import { Guid } from "guid-typescript";
import { iApprovers, DocumentChangeNotice, ApproverDetail, AttachementDetails, Attendance, IItem2, AuthorizationCapitalTransaction, BusinessUnitApps, ActLocation } from "../items/item";
import { Observable, of } from "rxjs";
import { IUser } from "../items/User";
import { startWith, map } from "rxjs/operators";
import references from '../../assets/references.json';
import { ModalComponent } from "../shared/modal/modal.component";


@Component({
  selector: 'entry-form',
  templateUrl: './entry.component.html'
})


export class EntryComponent implements OnInit {

  fileListAffected: File[] = [];
  listOfFilesAffected: AttachementDetails[] = [];
  listOfFilesAffectedSaved: AttachementDetails[] = [];
  withAttachmentAffected = false;

  buAppDetail: BusinessUnitApps[] = [];

  newAttachment: boolean = false;
  attachementDetails: AttachementDetails;
  businessUnits: IItem2[] = [];

  public id: string;
  param: number;
  heroForm: FormGroup;
  currentStringDate: string;
  filteredEmpName: Observable<IUser[]>;
  userEmpList: any[] = [];
  businessUnitList: any[] = [];
  locationList: ActLocation[] = [];
  entList: ActLocation[] = [];
  clsList: ActLocation[] = [];
  regionList: Array<string> = [];
  entityList: Array<string> = [];
  classificationList: Array<string> = [];
  investmentCategoryList: any[] = [];
  budgetCategoryList: any[] = [];
  selregion: string;
  selentity: string;
  apploc: string;

  @ViewChild('attachments') attachment: any;
  fileList: File[] = [];
  listOfFiles: AttachementDetails[] = [];
  listOfFilesSaved: AttachementDetails[] = [];
  withAttachment = false;
  errorMessage = '';
  @ViewChild('attachmentsAffected') attachmentsAffected: any;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder,
    private authService: AuthService,
    private itemService: itemService) {
    this.id = Guid.create().toString();
  }
  @ViewChild(ModalComponent) child;
  ngAfterViewInit() {
    setTimeout(() => {
      this.child.popupMessage = 'tessss';
      this.child.redirectUrl = '/view';
    });
  }
  ngOnInit() {
    // this.regionList = ['Asia Pacific', 'EMEA', 'North America', 'Latam'];
    // this.entityList = ['TOI', 'TOL','TOB','TOS','TOPL','TOPI','TOTL'];
    this.currentStringDate = new Date().toISOString().substring(0, 10);

    this.heroForm = this.formBuilder.group({
      id: [0],
      projectName: ['', Validators.required],
      projectManagerName: ['', Validators.required],
      // projectManagerEmail: ['', Validators.required],
      projectManagerEmail: [''],
      location: [''],
      region: ['', Validators.required],
      entity: ['', Validators.required],
      classification: ['', Validators.required],
      projectDescription: ['', Validators.required],
      startContruction: ['', Validators.required],
      startUpDate: ['', Validators.required],
      economicLife: [null, Validators.required],
      projectIRR: [0],
      projectCashPaybackYears: [''],
      projectROC: [0],
      actAmount: [null, Validators.required],
      actNumber: [''],
      doesNeedEHSReview: [false],
      invesmentCategory: ['', Validators.required],
      budgetCategory: ['', Validators.required],
      actAccountNumber: [''],
      //added for api to run   
      actRecordedtoMFGPRO: [false],
      approvalInProgress: [false],
      status: [''],
      created: [this.currentStringDate],
      emailSentForACTNoAssigned: [false],
      emailSentForACTRecordedMFG: [false],
      gcsCreated: [false],
      modified: [this.currentStringDate],
      reRunWorkflowProcess: [false],
      createdBy: [''],
      createdByEmail: [this.authService.email],
      modifiedBy: [''],
      ehsApproverEmail: [''],
      ehsReviewerEmail: [''],
      spReviewerEmail: [''],
      businessUnitApprover1Email: [''],
      financeApprover1Email: [''],
      businessUnitApprover2Email: [''],
      globalFinanceApprover1Email: [''],
      finalApprover5Email: [''],
      finalApprover6Email: [''],
      financeEmail: [''],

      purchasingEmail: [''],

      wfCurrentApprover: [''],
      wfCurrentApproverEmail: [''],
      wfCurrentApproverName: [''],
      wfehsApprover: [''],
      wfehsApproverApproval: [''],
      wfehsApproverComment: [''],
      wfehsReviewer: [''],
      wfehsReviewerApproval: [''],
      wfehsReviewerComment: [''],
      wfspReviewer: [''],
      wfspReviewerApproval: [''],
      wfspReviewerComment: [''],
      wfbU1Approver: [''],
      wfbU1ApproverApproval: [''],
      wfbU1ApproverComment: [''],
      wfFinanceApprover1: [''],
      wfFinanceApprover1Approval: [''],
      wfFinanceApprover1Comment: [''],
      wfbU2Approver: [''],
      wfbU2ApproverApproval: [''],
      wfbU2ApproverComment: [''],
      wfGlobalFinanceApprover1: [''],
      wfGlobalFinanceApprover1Approval: [''],
      wfGlobalFinanceApprover1Comment: [''],
      wfFinalApprover5: [''],
      wfFinalApprover5Approval: [''],
      wfFinalApprover5Comment: [''],
      wfFinalApprover6: [''],
      wfFinalApprover6Approval: [''],
      wfFinalApprover6Comment: ['']

      // finalApprover1Email: [''],
      // finalApprover2Email: [''],
      // finalApprover3Email: [''],
      // finalApprover4Email: [''],
      // wfFinalApprover1: [''],
      // wfFinalApprover1Approval: [''],
      // wfFinalApprover1Comment: [''],
      // wfFinalApprover2: [''],
      // wfFinalApprover2Approval: [''],
      // wfFinalApprover2Comment: [''],
      // wfFinalApprover3: [''],
      // wfFinalApprover3Approval: [''],
      // wfFinalApprover3Comment: [''],
      // wfFinalApprover4: [''],
      // wfFinalApprover4Approval: [''],
      // wfFinalApprover4Comment: [''],

    });

    this.param = Number(this.route.snapshot.paramMap.get('id'));
    if (this.param) {
      const id = this.param;
      this.getItem(id);
    }
    else {
      this.param = 0;
    }
    //console.log(this.heroForm.controls['financeTEAuditExceptionsDetails'].value); 
    this.child.showLoading = true;
    this.itemService.getEmployee().subscribe(
      res => {
        this.userEmpList = res;
        this.child.showLoading = false;
      });
    //console.log(references);


    // let currentYear = (new Date()).getFullYear();

    this.filteredEmpName = this.heroForm.controls['projectManagerName'].valueChanges
      .pipe(startWith(''),
        map(
          user =>
            this._filterUser(user, this.userEmpList)
        )
      );

    this.itemService.getBusinessUnit().subscribe(
      res => {
        this.businessUnitList = res;
      });
    // this.itemService.getClassification().subscribe(
    //   res => {
    //     this.classificationList = res;
    //   });
    this.itemService.getInvestmentCategory().subscribe(
      res => {
        this.investmentCategoryList = res;
      });
    this.itemService.getBugetCategory().subscribe(
      res => {
        this.budgetCategoryList = res;
      });

    this.itemService.getactlocation().subscribe(
      res => {
        this.locationList = res.filter(x => x.acttype.includes('region'));
        this.entList = res.filter(x1 => x1.acttype.includes('entity'));
        this.clsList = res.filter(x2 => x2.acttype.includes('classification'));
        for (let i = 0; i < this.locationList.length; i++) {
          this.addregion(this.locationList[i].title);
        }
        for (let j = 0; j < this.entList.length; j++) {
          this.addentity(this.entList[j].title);
        }
        for (let k = 0; k < this.clsList.length; k++) {
          this.addclassification(this.clsList[k].title);
        }

      }
    );
  }

  addregion(item) {
    if (this.regionList.indexOf(item) === -1) {
      this.regionList.push(item);
    }

  }
  addentity(item) {
    if (this.entityList.indexOf(item) === -1) {
      this.entityList.push(item);
    }

  }
  addclassification(item) {
    if (this.classificationList.indexOf(item) === -1) {
      this.classificationList.push(item);
    }

  }
  private getItem(id: number) {
    this.itemService.getItem(id).subscribe(
      item => {
        this.heroForm.patchValue(item);

        this.itemService.getAttachmentDetails(item.id).subscribe(
          res => {
            this.listOfFilesAffectedSaved = res.filter(x1 => x1.additionalInfo.includes('FinanceACT'));

            if (this.listOfFilesSaved.length)
              this.withAttachment = true;
          }
        )


      },
      error => this.errorMessage = <any>error);


  }
  private _filterUser(value: string, userVar: any[]): IUser[] {
    const filterValue = value != undefined ? value.toLowerCase() : '';
    return userVar.filter((item: IUser) =>
      item.name.toLocaleLowerCase().indexOf(filterValue) !== -1);
  }

  onOk() {
    this.child.showLoading = true;
    if (this.heroForm.value.id > 0) {
      this.itemService.editItem(this.heroForm.value.id, this.heroForm.value).subscribe(res2 => {
        var response = res2['_body'];
        console.log(response);
        this.attachmentAffectedMethod(this.param.toString());
        this.child.showLoading = false;
        this.child.popupMessage = 'Successfully Saved!'
        this.child.redirectUrl = ['/view'];
        this.child.showMessage = true;
      },
        err => {
          this.child.showLoading = false;
          this.child.redirectUrl = '';
          this.child.popupMessage = 'Error Occurred. Please try again.';
          this.child.showMessage = true;
        });
    }
    else {
      this.heroForm.controls['createdBy'].setValue(this.authService.name);
      this.heroForm.controls['createdByEmail'].setValue(this.authService.email);
      this.heroForm.controls['modifiedBy'].setValue(this.authService.name);
      this.heroForm.controls['status'].setValue("Pending");

      if (this.heroForm.value.doesNeedEHSReview == false) {
        this.heroForm.controls['wfCurrentApprover'].setValue("S&PReviewer");
        this.heroForm.controls['wfCurrentApproverEmail'].setValue(this.heroForm.value.spReviewerEmail);
        this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfspReviewer);
      }

      console.log(this.heroForm.value);
      this.itemService.addItem(this.heroForm.value).subscribe(res2 => {
        var response = JSON.parse(res2['_body']);
        this.attachmentAffectedMethod(response.id);
        this.child.showLoading = false;
        this.child.popupMessage = 'Successfully Saved!'
        this.child.redirectUrl = ['/view'];
        this.child.showMessage = true;
      },
        err => {
          this.child.showLoading = false;
          this.child.redirectUrl = '';
          this.child.popupMessage = 'Error Occurred. Please try again.';
          this.child.showMessage = true;
        });
    }
  }

  onchangeLocation() {

    this.heroForm.controls['ehsApproverEmail'].setValue("");
    this.heroForm.controls['wfehsApprover'].setValue("");
    this.heroForm.controls['ehsReviewerEmail'].setValue("");
    this.heroForm.controls['wfehsReviewer'].setValue("");
    this.heroForm.controls['globalFinanceApprover1Email'].setValue("");
    this.heroForm.controls['wfGlobalFinanceApprover1'].setValue("");
    this.heroForm.controls['financeApprover1Email'].setValue("");
    this.heroForm.controls['wfFinanceApprover1'].setValue("");
    this.heroForm.controls['businessUnitApprover1Email'].setValue("");
    this.heroForm.controls['wfbU1Approver'].setValue("");
    this.heroForm.controls['businessUnitApprover2Email'].setValue("");
    this.heroForm.controls['wfbU2Approver'].setValue("");
    this.heroForm.controls['spReviewerEmail'].setValue("");
    this.heroForm.controls['wfspReviewer'].setValue("");
    // this.heroForm.controls['finalApprover1Email'].setValue("");
    // this.heroForm.controls['wfFinalApprover1'].setValue("");
    // this.heroForm.controls['finalApprover2Email'].setValue("");
    // this.heroForm.controls['wfFinalApprover2'].setValue("");
    // this.heroForm.controls['finalApprover3Email'].setValue("");
    // this.heroForm.controls['wfFinalApprover3'].setValue("");
    // this.heroForm.controls['finalApprover4Email'].setValue("");
    // this.heroForm.controls['wfFinalApprover4'].setValue("");
    this.heroForm.controls['finalApprover5Email'].setValue("");
    this.heroForm.controls['wfFinalApprover5'].setValue("");
    this.heroForm.controls['finalApprover6Email'].setValue("");
    this.heroForm.controls['wfFinalApprover6'].setValue("");
    this.heroForm.controls['wfCurrentApprover'].setValue("");

    var reg = this.heroForm.value.region;
    var ent = this.heroForm.value.entity;
    var cls = this.heroForm.value.classification;

    if (!(reg == "" || ent == "" || cls == "")) {
      if (ent == "TOS" || ent == "TOPL") {
        this.heroForm.controls['wfFinanceApprover1'].setValue("Lumibao, Ronald");
        this.heroForm.controls['financeApprover1Email'].setValue("rlumibao@transitions.com");
        // this.heroForm.controls['wfFinanceApprover1'].setValue("Silva, Coren");
        // this.heroForm.controls['financeApprover1Email'].setValue("coren.silva@transitions.com");

        if (reg == "Asia Pacific") {

          this.apploc = "Asia Pacific Region (Commercial) - TOS (Singapore), TOPL (Australia)";
        }
        else {
          if (cls == "GE") {
            this.apploc = "Global Engineering";
          }
          else if (cls == "IT") {
            this.apploc = "IT";
          }
          else if (cls == "R&D") {
            this.apploc = "RandD";
          }
          else if (cls == "HR") {
            this.apploc = "HR";
          }
          else if (cls == "Marketing") {
            this.apploc = "Marketing";
          }
          else {
            this.apploc = "Asia Pacific Region (Commercial) - TOS (Singapore), TOPL (Australia)";
          }
        }
      }
      else if (ent == "TOPI") {
        this.heroForm.controls['wfFinanceApprover1'].setValue("Corvera, Mari-Dhen");
        this.heroForm.controls['financeApprover1Email'].setValue("mcorvera@transitions.com");
        // this.heroForm.controls['wfFinanceApprover1'].setValue("Silva, Coren");
        // this.heroForm.controls['financeApprover1Email'].setValue("coren.silva@transitions.com");
        this.apploc = "Asia Pacific Region (Ops. Excellence) - TOPI Philippines";
      }
      else if (ent == "TOTL") {
        this.heroForm.controls['wfFinanceApprover1'].setValue("Wanvisa SUKRAST");
        this.heroForm.controls['financeApprover1Email'].setValue("wanvisa.sukrast@transitions.com");
        // this.heroForm.controls['wfFinanceApprover1'].setValue("Silva, Coren");
        // this.heroForm.controls['financeApprover1Email'].setValue("coren.silva@transitions.com");
        this.apploc = "Asia Pacific Region (Ops. Excellence) - TOTL Thailand";
      }
      else if (ent == "TOL") {
        this.heroForm.controls['wfFinanceApprover1'].setValue("Kelly, David");
        this.heroForm.controls['financeApprover1Email'].setValue("david.kelly@transitions.com");
        // this.heroForm.controls['wfFinanceApprover1'].setValue("Silva, Coren");
        // this.heroForm.controls['financeApprover1Email'].setValue("coren.silva@transitions.com");
        if (reg == "EMEA") {
          if (cls == "Operations") {
            this.apploc = "EMEA Region (Ops. Excellence) - TOL Ireland";
          }
          else if (cls == "Commercial") {
            this.apploc = "EMEA Region (Commercial) - TOL Ireland";
          }
          else if (cls == "GE") {
            this.apploc = "Global Engineering";
          }
          else if (cls == "IT") {
            this.apploc = "IT";
          }
          else if (cls == "R&D") {
            this.apploc = "RandD";
          }
          else if (cls == "HR") {
            this.apploc = "HR";
          }
          else if (cls == "Marketing") {
            this.apploc = "Marketing";
          }
        } else {
          if (cls == "GE") {
            this.apploc = "Global Engineering";
          }
          else if (cls == "IT") {
            this.apploc = "IT";
          }
          else if (cls == "R&D") {
            this.apploc = "RandD";
          }
          else if (cls == "HR") {
            this.apploc = "HR";
          }
          else if (cls == "Marketing") {
            this.apploc = "Marketing";
          }
          else if (cls == "Operations") {
            this.apploc = "EMEA Region (Ops. Excellence) - TOL Ireland";
          }
          else if (cls == "Commercial") {
            this.apploc = "EMEA Region (Commercial) - TOL Ireland";
          }
        }
      }
      else if (ent == "TOI") {
        this.heroForm.controls['wfFinanceApprover1'].setValue("Wilder, Mary");
        this.heroForm.controls['financeApprover1Email'].setValue("mwilder@transitions.com");
        // this.heroForm.controls['wfFinanceApprover1'].setValue("Silva, Coren");
        // this.heroForm.controls['financeApprover1Email'].setValue("coren.silva@transitions.com");
        if (reg == "North America") {
          if (cls == "Operations") {
            this.apploc = "NA Region (Ops. Excellence) - TOI Pinellas Park";
          }
          else if (cls == "Commercial") {
            this.apploc = "NA Region (Commercial) - TOI Pinellas Park";
          }
          else if (cls == "GE") {
            this.apploc = "Global Engineering";
          }
          else if (cls == "IT") {
            this.apploc = "IT";
          }
          else if (cls == "R&D") {
            this.apploc = "RandD";
          }
          else if (cls == "HR") {
            this.apploc = "HR";
          }
          else if (cls == "Marketing") {
            this.apploc = "Marketing";
          }
        } else {
          if (cls == "GE") {
            this.apploc = "Global Engineering";
          }
          else if (cls == "IT") {
            this.apploc = "IT";
          }
          else if (cls == "R&D") {
            this.apploc = "RandD";
          }
          else if (cls == "HR") {
            this.apploc = "HR";
          }
          else if (cls == "Marketing") {
            this.apploc = "Marketing";
          }
          else if (cls == "Operations") {
            this.apploc = "NA Region (Ops. Excellence) - TOI Pinellas Park";
          }
          else if (cls == "Commercial") {
            this.apploc = "NA Region (Commercial) - TOI Pinellas Park";
          }


        }
      }
      else if (ent == "TOB") {
        this.heroForm.controls['wfFinanceApprover1'].setValue("Tozzi, Maristela");
        this.heroForm.controls['financeApprover1Email'].setValue("mtozzi@transitions.com");
        // this.heroForm.controls['wfFinanceApprover1'].setValue("Silva, Coren");
        // this.heroForm.controls['financeApprover1Email'].setValue("coren.silva@transitions.com");
        if (reg == "LATAM") {
          this.apploc = "SA Region (Commercial) - TOBL Brazil";
        } 
        else {
          if (cls == "GE") {
            this.apploc = "Global Engineering";
          }
          else if (cls == "IT") {
            this.apploc = "IT";
          }
          else if (cls == "R&D") {
            this.apploc = "RandD";
          }
          else if (cls == "HR") {
            this.apploc = "HR";
          }
          else if (cls == "Marketing") {
            this.apploc = "Marketing";
          }
          else {
            this.apploc = "SA Region (Commercial) - TOBL Brazil";
          }
        }
      }
      else if(ent == "TEST"){
        this.heroForm.controls['wfFinanceApprover1'].setValue("Silva, Coren");
        this.heroForm.controls['financeApprover1Email'].setValue("coren.silva@transitions.com");
        this.apploc = "FOR TESTING";

        const regionControl = this.heroForm.get('region');
        const classCtrl = this.heroForm.get('classification');
        regionControl.setValidators(null);
        regionControl.updateValueAndValidity();
        classCtrl.setValidators(null);
        classCtrl.updateValueAndValidity();
      }

      // console.log(this.apploc);
      this.heroForm.controls['location'].setValue(this.apploc);
      this.getlocation(this.apploc);
      
    }
  }

  private getlocation(location: string) {
    this.itemService.getApprovers(location).subscribe(
      result => {
        this.buAppDetail = result;
        console.log(location);
        console.log(this.buAppDetail);
        for (var i = 0; i < this.buAppDetail.length; i++) {

          this.heroForm.controls['ehsApproverEmail'].setValue(this.buAppDetail[i].ehsApproverEmail);
          this.heroForm.controls['wfehsApprover'].setValue(this.buAppDetail[i].ehsApproverName);
          this.heroForm.controls['ehsReviewerEmail'].setValue(this.buAppDetail[i].ehsReviewerEmail);
          this.heroForm.controls['wfehsReviewer'].setValue(this.buAppDetail[i].ehsReviewerName);
          this.heroForm.controls['globalFinanceApprover1Email'].setValue(this.buAppDetail[i].globalFinanceApprover1Email);
          this.heroForm.controls['wfGlobalFinanceApprover1'].setValue(this.buAppDetail[i].globalFinanceApprover1Name);
          //this.heroForm.controls['financeApprover1Email'].setValue(this.buAppDetail[i].financeApprover1Email);
          // this.heroForm.controls['wfFinanceApprover1'].setValue(this.buAppDetail[i].financeApprover1Name);

          this.heroForm.controls['businessUnitApprover1Email'].setValue(this.buAppDetail[i].businessUnitApprover1Email);
          this.heroForm.controls['wfbU1Approver'].setValue(this.buAppDetail[i].businessUnitApprover1Name);
          this.heroForm.controls['businessUnitApprover2Email'].setValue(this.buAppDetail[i].businessUnitApprover2Email);
          this.heroForm.controls['wfbU2Approver'].setValue(this.buAppDetail[i].businessUnitApprover2Name);
          this.heroForm.controls['spReviewerEmail'].setValue(this.buAppDetail[i].spReviewerEmail);
          this.heroForm.controls['wfspReviewer'].setValue(this.buAppDetail[i].spReviewerName);
          // this.heroForm.controls['finalApprover1Email'].setValue(this.buAppDetail[i].finalApprover1Email);
          // this.heroForm.controls['wfFinalApprover1'].setValue(this.buAppDetail[i].finalApprover1Name);
          // this.heroForm.controls['finalApprover2Email'].setValue(this.buAppDetail[i].finalApprover2Email);
          // this.heroForm.controls['wfFinalApprover2'].setValue(this.buAppDetail[i].finalApprover2Name);
          // this.heroForm.controls['finalApprover3Email'].setValue(this.buAppDetail[i].finalApprover3Email);
          // this.heroForm.controls['wfFinalApprover3'].setValue(this.buAppDetail[i].finalApprover3Name);
          // this.heroForm.controls['finalApprover4Email'].setValue(this.buAppDetail[i].finalApprover4Email);
          // this.heroForm.controls['wfFinalApprover4'].setValue(this.buAppDetail[i].finalApprover4Name);
          this.heroForm.controls['finalApprover5Email'].setValue(this.buAppDetail[i].finalApprover5Email);
          this.heroForm.controls['wfFinalApprover5'].setValue(this.buAppDetail[i].finalApprover5Name);
          this.heroForm.controls['finalApprover6Email'].setValue(this.buAppDetail[i].finalApprover6Email);
          this.heroForm.controls['wfFinalApprover6'].setValue(this.buAppDetail[i].finalApprover6Name);

          if (this.heroForm.value.wfehsApprover != null && this.heroForm.value.wfehsApproverApproval == "") {

            this.heroForm.controls['wfCurrentApprover'].setValue("EHSApprover");
            this.heroForm.controls['wfCurrentApproverEmail'].setValue(this.heroForm.value.ehsApproverEmail);
            this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfehsApprover);
          }
          else if (this.heroForm.value.wfehsReviewer != null && this.heroForm.value.wfehsReviewerApproval == "") {
            this.heroForm.value.wfCurrentApprover = "EHSReviewer";
            this.heroForm.controls['wfCurrentApproverName'].setValue(this.heroForm.value.wfehsReviewer);
          }
          console.log(this.heroForm.value);

        }
      }
    );
  }
  onFileChangeAffected(event: any) {
    for (var i = 0; i <= event.target.files.length - 1; i++) {
      var selectedFile = event.target.files[i];
      this.fileListAffected.push(selectedFile);
      this.listOfFilesAffected.push(selectedFile.name)
    }
    this.attachmentsAffected.nativeElement.value = '';
    this.withAttachmentAffected = true;
  }

  removeSelectedFileAffected(index) {
    // Delete the item from fileNames list
    this.listOfFilesAffected.splice(index, 1);
    // delete file from FileList
    this.fileListAffected.splice(index, 1);
  }

  attachmentAffectedMethod(refID: string): void {
    const formData = new FormData();
    for (var i = 0; i <= this.fileListAffected.length - 1; i++) {
      this.newAttachment = true;
      var selectedFile = this.fileListAffected[i];
      formData.append('Files', selectedFile);
      formData.append('requestID', refID.toString());

      this.attachementDetails = new AttachementDetails(
        0,
        selectedFile.name,
        refID.toString(),
        "FinanceACT"
      )

      this.attachementDetails.filename = selectedFile.name;
      this.itemService.addAttachmentsDetails(this.attachementDetails)
    }

    if (this.newAttachment)
      this.itemService.uploadAttachmentFile(formData);
  }

  downloadAttachment(_filename: string, _folderID: string): void {
    this.itemService.downloadAttachment(_filename, _folderID)
      .subscribe(x => {
        // It is necessary to create a new blob object with mime-type explicitly set
        // otherwise only Chrome works like it should
        var newBlob = new Blob([x], { type: 'application/octet-stream' });

        // IE doesn't allow using a blob object directly as link href
        // instead it is necessary to use msSaveOrOpenBlob
        if (window.navigator && window.navigator.msSaveOrOpenBlob) {
          window.navigator.msSaveOrOpenBlob(newBlob);
          return;
        }

        // For other browsers: 
        // Create a link pointing to the ObjectURL containing the blob.
        const data = window.URL.createObjectURL(newBlob);

        var link = document.createElement('a');
        link.href = data;
        link.href = data;
        link.download = _filename;
        // this is necessary as link.click() does not work on the latest firefox
        link.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true, view: window }));

      });
  }

  deleteAttachedAffectedFileSaved(index, filename: string) {
    // Delete the item from fileNames list
    if (confirm("Are you sure to delete this file? This file will directly remove from the server.")) {
      let attach = this.listOfFilesAffectedSaved.find(x => x.filename === filename);
      this.listOfFilesAffectedSaved.splice(index, 1);
      this.itemService.DeleteAttachmentDetails(attach.id.toString());
      this.itemService.DeleteAttachmenfile(filename, this.param.toString());
    }
  }

  onDelete(id) {
    if (confirm("Are you sure to delete this ACT?")) {
      this.itemService.DeleteItem(this.param).subscribe(res2 => {
        this.child.showLoading = false;
        this.child.popupMessage = 'Successfully Deleted!'
        this.child.redirectUrl = ['/view'];
        this.child.showMessage = true;
      },
        err => {
          this.child.showLoading = false;
          this.child.redirectUrl = '';
          this.child.popupMessage = 'Error Occurred. Please try again.';
          this.child.showMessage = true;
        });
    }
  }


  onCancel() {
    //this.child.showLoading = true;
    this.router.navigateByUrl('/view');
  }
  onResend(){
    this.itemService.sendNotification(this.heroForm.value.id, this.heroForm.value);
  }
}
