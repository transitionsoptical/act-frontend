import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TemplateFormComponent } from './templateForm/templateForm.component';
import { ReactiveFormComponent } from './reactiveForm/reactiveForm.component';
import { StarterReactiveFormComponent } from './starterReactiveForm/starterReactiveForm.component';
import { UpdateOnComponent } from './updateOn/updateOn.component';
import { ControlValueAccessorComponent } from './controlValueAccessor/controlValueAccessor.component';
import { ReactiveDynamicFormComponent } from './reactiveDynamicForm/reactive-dynamic-form.component';
import { viewTemplateFormComponent } from './viewTemplateForm/viewTemplateForm.component';
import { AuthGuard } from "./services/auth.guard";
import { SecureInnerPagesGuard } from "./services/secure-inner-pages.guard"
import { StarterTemplateFormComponent } from './starterTemplateForm/starterTemplateForm.component';
import { EntryComponent } from './Forms/entry.component';
import { ApprovalComponent } from './Forms/approval.component'; 
import { DisplayComponent } from './Forms/display.component';
import { viewFilteredFormComponent } from './viewTemplateForm/viewFilteredForm.component';


const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: '/view' },
  { path: 'login', component: StarterTemplateFormComponent },
  { path: 'starterreactiveform', component: StarterReactiveFormComponent },
  { path: 'item', component: EntryComponent, canActivate: [SecureInnerPagesGuard] },
  // { path: 'entry/:id',  component: EntryComponent, canActivate: [AuthGuard]  },
  { path: 'entry/:id', component: EntryComponent, canActivate: [AuthGuard] },//, canActivate: [AuthGuard]
  { path: 'approval/:id', component: ApprovalComponent, canActivate: [AuthGuard]   },//,canActivate: [AuthGuard]
 // { path: 'approval', component: ApprovalComponent, canActivate: [AuthGuard] },
 { path: 'approval', component: ApprovalComponent },
  { path: 'templateform', component: TemplateFormComponent },
  { path: 'reactiveform', component: ReactiveFormComponent },
  { path: 'updateon', component: UpdateOnComponent },
  //{ path: 'entry', component: EntryComponent, canActivate: [AuthGuard] }, 
  { path: 'entry', component: EntryComponent}, 
  { path: 'controlvalueaccessor', component: ControlValueAccessorComponent },
  { path: 'reactivedynamicform', component: ReactiveDynamicFormComponent },
  { path: 'view', component: viewTemplateFormComponent, canActivate: [AuthGuard]  },
  { path: 'displayForm/:id', component: DisplayComponent, canActivate: [AuthGuard] },
  { path: 'viewfilter', component: viewFilteredFormComponent, canActivate: [AuthGuard] },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
  static components = [
    StarterTemplateFormComponent, StarterReactiveFormComponent,
    TemplateFormComponent, ReactiveFormComponent,
    ControlValueAccessorComponent, UpdateOnComponent, ReactiveDynamicFormComponent, viewTemplateFormComponent,
    EntryComponent, ApprovalComponent, DisplayComponent, viewFilteredFormComponent
  ];
}

