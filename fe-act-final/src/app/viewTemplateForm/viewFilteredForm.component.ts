import { Component, ViewChild, AfterViewInit } from "@angular/core";
import { IItem } from "../items/item";
import { itemService } from "../items/item.service";
import { Router } from '@angular/router';
import { AuthService } from "../services/auth.service";
import { ModalComponent } from "../shared/modal/modal.component";

@Component({
    selector: 'view-template-form',
    templateUrl: './viewFilteredForm.component.html',
    providers: [itemService]
})
export class viewFilteredFormComponent {

    _listFilter: string;
    userEmpList: any[] = [];
    get listFilter(): string {
        return this._listFilter;
    }
    set listFilter(value: string) {
        this._listFilter = value;
        this.filteredItems = this.listFilter ? this.performFilter(this.listFilter) : this.items;
    }

    filteredItems: IItem[];
    items: IItem[] = [];
    errorMessage: string;
    p: string;

    constructor(private itemService: itemService,
        private router: Router,
        private authService: AuthService) {
          

    }
    performFilter(filterBy: string): IItem[] {
        filterBy = filterBy.toLowerCase();
        return this.items.filter((item: IItem) =>
            (item.projectName == null ? '' : item.projectName).toLocaleLowerCase().indexOf(filterBy) !== -1);

    }
    @ViewChild(ModalComponent) child;

    ngOnInit(): void {
        
        this.itemService.getEmployee().subscribe(
            res => {
                this.userEmpList = res;
                console.log(this.userEmpList);

                this.loadData();
            });

       
    }

    loadData(): void {
        this.child.showLoading = true;

        

        // let x = this.userEmpList.filter(x => x.email == this.authService.email)
        let x = this.userEmpList.filter(x => x.email == this.authService.email)
        if (x.length > 0) {
           console.log(x[0].name);

            this.itemService.getFilteredItems(x[0].name).subscribe(
                items => {
                    // if (this.authService.isAdmin)
                    this.items = items;
                    console.log(this.items);
                    this.filteredItems = this.items;
                    this.child.showLoading = false;
                },
                error => this.errorMessage = <any>error
            );
        }
    } 
    newItem(): void {
        this.router.navigate(['/entry'])
    }

    btnRoute1(id: string): void {
        this.router.navigateByUrl('/entry/' + id);

    };
    btnRoute(id: string): void {
        this.router.navigateByUrl('/displayForm/' + id);
    };
    onViewAll(): void
    {
        this.router.navigate(['/view'])
    }

    report(): void{
        this.router.navigate(['/report'])
    }
    state: boolean = false;

    isAllowed = (createdbyemail) => {

        console.log(createdbyemail);
    return createdbyemail === this.authService.email ? true : this.state;
    }

}