import { Component, ViewChild, AfterViewInit } from "@angular/core";
import { IItem } from "../items/item";
import { itemService } from "../items/item.service";
import { Router } from '@angular/router';
import { AuthService } from "../services/auth.service";
import { ModalComponent } from "../shared/modal/modal.component";

@Component({
    selector: 'view-template-form',
    templateUrl: './viewTemplateForm.component.html',
    providers: [itemService]
})
export class viewTemplateFormComponent {

    _listFilter: string;
    get listFilter(): string {
        return this._listFilter;
    }
    set listFilter(value: string) {
        this._listFilter = value;
        this.filteredItems = this.listFilter ? this.performFilter(this.listFilter) : this.items;
    }

    filteredItems: IItem[];
    items: IItem[] = [];
    errorMessage: string;
    p: string;

    constructor(private itemService: itemService,
        private router: Router,
        private authService: AuthService) {


    }
    performFilter(filterBy: string): IItem[] {
        filterBy = filterBy.toLowerCase();
        return this.items.filter((item: IItem) =>
            (item.projectName == null ? '' : item.projectName).toLocaleLowerCase().indexOf(filterBy) !== -1);

    }
    @ViewChild(ModalComponent) child;
    ngOnInit(): void {
        this.loadData();
    }

    loadData(): void {
        this.child.showLoading = true;
        this.itemService.getItems().subscribe(
            items => {
                 if (this.authService.isAdmin)
                    this.items = items;
                console.log(this.items);
                this.filteredItems = this.items;



                this.child.showLoading = false;
            },
            error => this.errorMessage = <any>error
        );
    }

    newItem(): void {
        this.router.navigate(['/entry'])
    }

    onViewPendings(){
        this.router.navigate(['/viewfilter'])
    }

    btnRoute1(id: string): void {
        this.router.navigateByUrl('/entry/' + id);

    };
    btnRoute(id: string): void {
        this.router.navigateByUrl('/displayForm/' + id);
    };

    report(): void{
        this.router.navigate(['/report'])
    }
    state: boolean = false;

    isAllowed = (createdbyemail) => {

        console.log(createdbyemail);
    return createdbyemail === this.authService.email ? true : this.state;
    }

}