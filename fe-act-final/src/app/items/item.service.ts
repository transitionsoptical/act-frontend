import { Injectable } from "@angular/core";
import { IItem, AttachmentInfo, IAttachementDetails, AttachementDetails, DocumentChangeNotice, iApprovers, ApproverDetail, Attendance, IItem2, AuthorizationCapitalTransaction } from "./item";
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Observable, throwError } from "rxjs";
import { catchError, tap, map, share } from "rxjs/operators";
import { IncidentInvestigation } from "./incidentInvestigation";
import { Http, Headers, RequestOptions } from '@angular/http';
import { Router } from "@angular/router";
import { IUserApplication } from "./User";


@Injectable({
  providedIn: 'root'
})

export class itemService {
  // 'https://api.myjson.com/bins/qjte4'
  ///api/items
  //
  //http://insight-core.transitions.com
  private itemUrl = "https://insightcore.transitions.com/act/api/financeacts";
  private putACTUrl = "https://insightcore.transitions.com/act/api/financeacts/";
  private urlsetApprovalStatus = "https://insightcore.transitions.com/act/api/SetApprovalStatus/";
  private bUUrl = "https://insightcore.transitions.com/act/api/getApprovers";
  private businessUnitUrl = "https://insightcore.transitions.com/act/api/financeactbusinessunits/";
  private updateUrl = "https://insightcore.transitions.com/act/api/updatefinanceact/";
  private getFilteredActs = "https://insightcore.transitions.com/act/api/getFilteredAct";
  private getactlocationurl = "https://insightcore.transitions.com/act/api/FinanceActLocations";
  private investmentCategoryUrl = "https://insightcore.transitions.com/act/api/financeactinvestmentcategories/";
  private budgetCategoryUrl = "https://insightcore.transitions.com/act/api/financeactbudgetcategories";
  private deleteACTUrl = "https://insightcore.transitions.com/act/api/deletefinanceacts/";
  private attachmentUrl = "https://insightcore.transitions.com/act/api/UploadFile";
  private attachmentDownload = "https://insightcore.transitions.com/act/api/download"
  private attachmentFileDelete = "https://insightcore.transitions.com/act/api/deleteFile"
  private attachmentDetails = "https://insightcore.transitions.com/act/api/AttachmentsUpload/";
  private attachmentDetailsFilter = "https://insightcore.transitions.com/act/api/attachmentsFilter/";
  private attachmentDetailsdelete = "https://insightcore.transitions.com/act/api/deleteattachment/";

  private employeeUrl = 'https://insight-core.transitions.com/api/EmployeeMasters/';
  private applicationPerUsers = "https://insight-core.transitions.com/api/GetApplicationPerUsers?ApplicationId=5";
  
  
  // private itemUrl = 'https://localhost:5001/api/financeacts';
  // private putACTUrl = 'https://localhost:5001/api/financeacts/';
  // private urlsetApprovalStatus = "https://localhost:5001/api/SetApprovalStatus/";
  // private updateUrl = "https://localhost:5001/api/updatefinanceact/";
  // private businessUnitUrl = 'https://localhost:5001/api/financeactbusinessunits/';
  // private getFilteredActs = "https://localhost:5001/api/getFilteredAct";
  // private bUUrl = "https://localhost:5001/api/getApprovers";
  // private getactlocationurl = "https://localhost:5001/api/FinanceActLocations";
  private sendNotificationUrl = "https://localhost:5001/api/SendNotification";
  headers = new Headers({
    'accept': 'application/json'
  });

  _attachmentInfo: AttachmentInfo;

  public imageListCache = [];
  itemID: string;

  constructor(private http: HttpClient,
    private http2: Http,
    private router: Router) {
  }
  getBusinessUnit(): Observable<any[]> {
    return this.http.get<any[]>(this.businessUnitUrl).pipe(
      //tap(data => console.log('All: ' + JSON.stringify(data))),
      catchError(this.handleError)
    );
  }
  // getEntity(region:string): Observable<any[]> {
  //   return this.http.get<any[]>(this.getentityurl + "?region=" + region ).pipe(
  //     //tap(data => console.log('All: ' + JSON.stringify(data))),
  //     catchError(this.handleError)
  //   );
  // }
  // getClassification(region:string, entity:string): Observable<any[]> {
  //   return this.http.get<any[]>(this.getclassificationurl + "?region=" + region + "&entity=" + entity).pipe(
  //     //tap(data => console.log('All: ' + JSON.stringify(data))),
  //     catchError(this.handleError)
  //   );
  // }
  getInvestmentCategory(): Observable<any[]> {
    return this.http.get<any[]>(this.investmentCategoryUrl).pipe(
      //tap(data => console.log('All: ' + JSON.stringify(data))),
      catchError(this.handleError)
    );
  }
  getactlocation(): Observable<any[]> {
    return this.http.get<any[]>(this.getactlocationurl).pipe(
      //tap(data => console.log('All: ' + JSON.stringify(data))),
      catchError(this.handleError)
    );
  }
  getBugetCategory(): Observable<any[]> {
    return this.http.get<any[]>(this.budgetCategoryUrl).pipe(
      //tap(data => console.log('All: ' + JSON.stringify(data))),
      catchError(this.handleError)
    );
  }
  getItems(): Observable<IItem[]> {
    return this.http.get<IItem[]>(this.itemUrl).pipe(
      //tap(data => console.log('All: ' + JSON.stringify(data))),
      catchError(this.handleError)
    );
  }

  getFilteredItems(loginName: string): Observable<IItem[]> {
    return this.http.get<IItem[]>(this.getFilteredActs + "?currentApprover=" +loginName).pipe(
      tap(data => console.log('All: ')),
      catchError(this.handleError)
    );
  }

  getItem(id: number): Observable<IItem | undefined> {
    return this.getItems().pipe(
      map((items: IItem[]) => items.find(p => p.id === id))
    );
  }
  
  getApprovers(loc: string):Observable< IItem2[]>{
    return this.http.get<IItem2 []>(this.bUUrl + "/?Title=" + loc).pipe(
      // tap(data => console.log('All: '+ JSON.stringify(data))),
      catchError(this.handleError)
    );
  }  

  getEmployee(): Observable<any[]> {
    return this.http.get<any[]>(this.employeeUrl).pipe(
      //tap(data => console.log('All: ' + JSON.stringify(data))),
      catchError(this.handleError)
    );
  }


  setApprovalStatus(id: number, value: AuthorizationCapitalTransaction) {
    let headers = new Headers();

    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({ headers: headers });

    console.log(options);
    console.log(JSON.stringify(value));
    this.http2.post(this.urlsetApprovalStatus, JSON.stringify(value), { headers: headers }
    )
      .subscribe(
        res => {
          console.log(res);
        },
        err => {
          console.log("Error occured");
        }
      );
  }
  sendNotification(id: number, value: AuthorizationCapitalTransaction) {
    let headers = new Headers();

    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({ headers: headers });

    console.log(options);
    console.log(JSON.stringify(value));
    this.http2.post(this.sendNotificationUrl, JSON.stringify(value), { headers: headers }
    )
      .subscribe(
        res => {
          console.log(res);
        },
        err => {
          console.log("Error occured");
        }
      );
  }

  getAttachmentDetails(attachmentRef: number): Observable<AttachementDetails[]> {
    return this.http.get<AttachementDetails[]>(this.attachmentDetailsFilter + "?itemID=" + attachmentRef).pipe(
      tap(data => console.log('All: ' + JSON.stringify(data))),
      catchError(this.handleError)
    );
  }

  DeleteAttachmentDetails(id: string) {
    this.http2.delete(this.attachmentDetailsdelete + id).subscribe();
  }

  getACTs(reqid: string): Observable<AuthorizationCapitalTransaction[]> {
    return this.http.get<AuthorizationCapitalTransaction[]>(this.putACTUrl + reqid).pipe(
      tap(data => console.log('All: ')),
      catchError(this.handleError)
    );
  }
  

  addItem(value: object): Observable<object> {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({ headers: headers });

    return this.http2.post(this.itemUrl, JSON.stringify(value), { headers: headers })

  }

  private handleError(err: HttpErrorResponse) {
    let errorMessage = ''
    if (err.error instanceof ErrorEvent) {
      errorMessage = "An error occured:  ${err.error.message}";
    } else {
      errorMessage = "Server returned code: $(err.status}, error message is: ${err.message}"
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }



  getApplicationUsers(): Observable<IUserApplication[]> {
    return this.http.get<IUserApplication[]>(this.applicationPerUsers).pipe(
      tap(data => console.log('All: ' + JSON.stringify(data))),
      catchError(this.handleError)
    );
  }

  getApplicationUsersLogin(_email: string): Observable<IUserApplication | undefined> {
    return this.getApplicationUsers().pipe(
      map((items: IUserApplication[]) => items.find(p => p.email === _email))
    );
  }


  getApplicationUsersAll(): Observable<IUserApplication[] | undefined> {
    return this.http.get<IUserApplication[]>(this.applicationPerUsers).pipe(
      tap(data => console.log('All: ' + JSON.stringify(data))),
      catchError(this.handleError)
    )
  }

  addAttachmentsDetails(value: AttachementDetails) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({ headers: headers });
    this.http2.post(this.attachmentDetails, JSON.stringify(value), { headers: headers })
      .subscribe(res => {
        console.log(res);
      },
        err => {
          console.log("Error occured");
        }
      );
  }

  uploadAttachmentFile(attachmentInfo: FormData) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({ headers: headers });
    this.http2.post(this.attachmentUrl, attachmentInfo)
      .subscribe(
        res => {
          console.log(res);

        },
        err => {
          console.log("Error occured");
        }
      );
  }
  public downloadAttachment(_fileName: string, _folderID: string): Observable<Blob> {
    //const options = { responseType: 'blob' }; there is no use of this
    // this.http refers to HttpClient. Note here that you cannot use the generic get<Blob> as it does not compile: instead you "choose" the appropriate API in this way.
    return this.http.get(this.attachmentDownload + "?filename=" + _fileName + "&folderID=" + _folderID, { responseType: 'blob' });
  }

  DeleteAttachmenfile(_filename: string, _filepath: string) {
    this.http2.delete(this.attachmentFileDelete + "?filename=" + _filename + "&filepath=" + _filepath).subscribe();
  }

  editItem(id: number, value: object): Observable<object> {
    let headers = new Headers();

    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({ headers: headers });

    console.log(options);
    console.log(JSON.stringify(value));
    return this.http2.put(this.updateUrl + id, JSON.stringify(value), { headers: headers }
    )

  }

  DeleteItem(id: number): Observable<object>{
    return this.http2.delete(this.deleteACTUrl + id);
  }
}